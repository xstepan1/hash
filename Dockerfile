FROM ubuntu:20.04

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true
COPY config/01-apt-override /etc/apt/apt.conf.d/01-apt-override
COPY config/custom.list /etc/apt/sources.list.d/custom.list


# update
RUN apt update -yq && apt -yq upgrade

# basic utilities
RUN apt install -yq \
    git \
    make \
    sudo \
    fakeroot \
    patch \
    bash \
    xz-utils \
    wget \
    build-essential \
    ffmpeg && \
    apt clean

# imagemagick
ADD https://www.imagemagick.org/download/ImageMagick.tar.gz /tmp/ImageMagick.tar.gz
RUN apt update -yq && apt build-dep -yq imagemagick
RUN cd /tmp && \
    tar xzvf ImageMagick.tar.gz && \
    DIR=$(ls -d ImageMagick-7.*) && \
    cd $DIR && \
    ./configure && \
    make && \
    make install && \
    ldconfig /usr/local/lib && \
    rm -rf /tmp/*

# x11
RUN apt install -yq \
    xvfb \
    xauth \
    xdotool \
    x11vnc \
    i3 \
    rxvt-unicode && \
    apt clean

# processing
ADD https://download.processing.org/processing-3.5.4-linux64.tgz /tmp/processing.tgz
RUN mkdir /processing && \
    cd /processing && \
    tar -xzvf /tmp/processing.tgz && \
    ln -st /usr/bin /processing/processing-3.5.4/processing-java && \
    rm -rf /tmp/*

# blender
ADD https://ftp.nluug.nl/pub/graphics/blender/release/Blender2.91/blender-2.91.2-linux64.tar.xz /tmp/blender.tar.xz
RUN mkdir /blender && cd /blender && \
    tar -xJvf /tmp/blender.tar.xz && \
    ln -st /usr/bin /blender/blender-2.91.2-linux64/blender && \
    rm -rf /tmp/*

# p5.js
RUN apt install -yq firefox npm
COPY config/user.js /tmp/user.js
RUN firefox --headless --CreateProfile hash && \
    PROFILE_DIR=$(echo /root/.mozilla/firefox/*.hash) && \
    mv /tmp/user.js $PROFILE_DIR/user.js
RUN npm install -g serve

# add configs
COPY config/i3.conf /root/.config/i3/config
COPY config/Xresources /root/.Xresources

# add sources
COPY scripts /scripts
COPY sketches /sketches
COPY blends /blends
COPY jslib /jslib
RUN chmod +x /scripts/hash.sh /scripts/testout.sh && ln -st / /scripts/hash.sh
