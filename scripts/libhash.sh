#!/bin/bash

LIBHASH=true
INDENT=0
LAYOUT_DEPTH=0
FAILED_SCRIPTS=()

hashsquash() {
    if [ ${#HASH} -ne 64 ]; then
        echo 1>&2 "The HASH variable does not have 64 characters."
    fi

    HRANDOM=0
    for i in {0..15}; do
        TMP=${HASH:$((i * 4)):$(( (i + 1) * 4 ))}
        HRANDOM=$(( ($HRANDOM ^ 0x$TMP) & 0xffff))
    done
}

hashinit() {
    export WIDTH=${WIDTH:-1920}
    export HEIGHT=${HEIGHT:-1080}
    export DISPLAY=:42
    Xvfb $DISPLAY -screen -0 800x600x16 &
    XVFB_PID=$!
    pdebug "Xvfb[$XVFB_PID] started at $DISPLAY."

    i3 2>/dev/null &

    if [ ! -d /out ]; then
        mkdir /out;
    fi

    export HASH=${HASH:-0000000000000000000000000000000000000000000000000000000000000000}
    hashsquash

    trap hashclean EXIT
}

hashclean() {
    kill -s 2 $XVFB_PID
    wait $XVFB_PID
}

hrandom() {
    HRANDOM=$(( (1664525 * ${HRANDOM:=1} + 1013904223) % 65536 ))
    export HRANDOM
}

hrandomcolor() {
    hrandom && R=$(( $HRANDOM % 256 ))
    hrandom && G=$(( $HRANDOM % 256 ))
    hrandom && B=$(( $HRANDOM % 256 ))
    COLOR="rgb($R,$G,$B)"
}

mkout() {
    OUT=$(mktemp "/tmp/$1.XXXXXXXX")
}

indentpush() {
    INDENT=$((INDENT + 4))
    if [ $INDENT -gt 16 ]; then
        INDENT=16
    fi
}

indentpop() {
    INDENT=$((INDENT - 4))
    if [ $INDENT -lt 0 ]; then
        INDENT=0
    fi
}

hashprint() {
    printf "%*s" $INDENT ""
    echo $@
}

pbase() {
    hashprint "$(tput bold)$(tput setaf 1)$1$(tput sgr0) by $(tput setaf 3)$2$(tput sgr0)"
}

peffect() {
    hashprint "$(tput bold)$(tput setaf 2)$@$(tput sgr0)"
}

playout() {
    hashprint "$(tput bold)$(tput setaf 4)$@$(tput sgr0)"
}

perror() {
    hashprint "$(tput bold)$(tput setaf 1)ERROR: $@$(tput sgr0)"
}

pdebug() {
    [ -n "$DEBUG" ] && hashprint "$@"
}

runp5() {
    hrandom

    serve -l 5000 "/" >/dev/null 2>&1 &
    SERVE_PID=$!

    P5HASH=$HRANDOM
    firefox -P hash "http://localhost:5000/sketches/$1/$1?hash=$P5HASH&width=$WIDTH&height=$HEIGHT" >/dev/null 2>&1 &

    FIREFOX_PID=$!

    OUT="/tmp/$1_$P5HASH.png"
    while [ ! -f $OUT ]; do
        sleep 1;
    done
    while ! identify $OUT >/dev/null 2>&1 ; do
        sleep 1;
    done
    kill $FIREFOX_PID
    kill $SERVE_PID
}

runpde() {
    hrandom

    PDEHASH=$HRANDOM
    processing-java --sketch=/sketches/$1 --run "$PDEHASH" "$WIDTH" "$HEIGHT" "/tmp" >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        perror Processing failed.
        FAILED_SCRIPTS+=("$1")
    fi
    OUT="/tmp/$1_$PDEHASH.png"
}

runim() {
    EXTENSION=$1
    shift
    OUT=$(mktemp "/tmp/magick.XXXXXXXX.$EXTENSION")
    pdebug "Running magick $@ $OUT"
    magick -seed $HRANDOM "$@" -strip $OUT
}

runblend() {
    mkout blender
    export OUT=$OUT.png
    BLEND=/blends/$1/$1.blend
    SCRIPT=/blends/$1/$1.py
    pdebug "Running blend '$1'"
    blender $BLEND --background --python $SCRIPT >/dev/null 2>&1
    if [ $? -ne 0 ]; then
        perror Blender failed.
        FAILED_SCRIPTS+=("$1")
    fi
    # remove dumb metadata
    runim png $OUT
}

runscript() {
    SCRIPT=$1
    shift
    indentpush
    pdebug "Running '$SCRIPT' $@"
    source $SCRIPT
    run $@
    pdebug "Output is '$OUT'"
    indentpop
}

runrandom() {
    DIR=$1
    shift
    hrandom
    SCRIPTS=($(ls $DIR))
    SCRIPT_COUNT=$(ls $DIR | wc -l)
    SCRIPT="$DIR/${SCRIPTS[$(( $HRANDOM % $SCRIPT_COUNT ))]}"
    runscript $SCRIPT $@
}

runbase() {
    runrandom /scripts/base $@
}

runeffect() {
    runrandom /scripts/effects $@
}

runlayout() {
    runrandom /scripts/layouts $@
}

runrecursive() {
    if [ $LAYOUT_DEPTH -eq 0 ]; then
        runbase
    else
        LAYOUT_DEPTH=$(($LAYOUT_DEPTH - 1))
        runlayout
        LAYOUT_DEPTH=$(($LAYOUT_DEPTH + 1))
    fi
}
