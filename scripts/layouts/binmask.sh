#!/bin/bash

source "/scripts/preamble.sh"

run() {
    playout "binmask"

    runbase
    runscript /scripts/effects/mask.sh $OUT
    MASK=$OUT

    runrecursive
    LHS=$OUT

    runrecursive
    RHS=$OUT

    runim png $LHS $RHS $MASK -composite
    runeffect $OUT
}
