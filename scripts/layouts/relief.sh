#!/bin/bash

source /scripts/preamble.sh

run() {
    playout "relief"
    runrecursive
    export RELIEF=$OUT
    runblend relief
    runeffect $OUT
}
