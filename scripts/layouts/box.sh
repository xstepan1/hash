#!/bin/bash

source /scripts/preamble.sh

run() {
    playout "box"
    runrecursive
    SIDE=$(( $WIDTH < $HEIGHT ? $WIDTH : $HEIGHT))
    runim png $OUT -gravity center -crop ${SIDE}x${SIDE}+0+0 +repage
    export PAINTING=$OUT
    runblend box
    runeffect $OUT
}
