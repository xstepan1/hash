#!/bin/bash

source /scripts/preamble.sh

run() {
    playout "multiply"

    runrecursive
    LHS=$OUT

    runrecursive
    RHS=$OUT

    runim png -compose multiply $LHS $RHS -composite
    runeffect $OUT
}
