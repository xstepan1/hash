#!/bin/bash

source /scripts/preamble.sh

run() {
    playout "quadblend"

    runrecursive
    I1=$OUT

    runrecursive
    I2=$OUT

    runrecursive
    I3=$OUT

    runrecursive
    I4=$OUT

    runim png -compose blend -define "compose:args=50,50" $I1 $I2 $I3 $I4 -layers flatten
    runeffect $OUT
}

