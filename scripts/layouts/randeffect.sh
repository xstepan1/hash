#!/bin/bash

source "/scripts/preamble.sh"

run() {
    playout "randeffect"
    runrecursive
    CURRENT=$OUT
    COUNT=$(( ($HRANDOM % 4) + 1))
    for i in $(seq 1 $COUNT); do
        runeffect $CURRENT
        CURRENT=$OUT
    done
}

