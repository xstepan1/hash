#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "hue"
    hrandom && HUE=$(( $HRANDOM % 80 ))
    runim png $1 -modulate "100,100,$HUE"
}
