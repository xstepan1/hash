#!/bin/bash

source /scripts/preamble.sh

# based on https://blog.jiayu.co/2019/05/edge-detection-with-imagemagick/

run() {
    peffect "edges"

    runim mpc $1 -colorspace gray -alpha remove
    runim png $OUT \
        -define morphology:compose=Lighten \
        -morphology Convolve 'Roberts:@' \
        -negate \
        -gaussian-blur 1x1 \
        -auto-threshold OTSU
}
