#!/bin/bash

source "/scripts/preamble.sh"

run() {
    peffect "fewcolor"
    runim png $1 -colors $(($HRANDOM % 15 + 2))
}
