#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "dots"
    runim png -size 10x10 xc: -draw 'circle 5,5 1,3' -channel RGB -negate -write mpr:spot +delete \
        $1 -size ${WIDTH}x${HEIGHT} tile:mpr:spot +swap -compose multiply -composite
}
