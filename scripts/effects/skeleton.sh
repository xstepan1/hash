#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "skeleton"
    runscript /scripts/effects/mask.sh $1
    runim png $OUT -morphology Thinning:-1 Skeleton
}
