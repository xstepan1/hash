#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "negate"
    runim png $1 -channel RGB -negate
}
