#!/bin/bash

source "/scripts/preamble.sh"

run() {
    peffect "mask"
    runim png $1 -contrast-stretch 1x1% -monochrome
}
