#!/bin/bash

source "/scripts/preamble.sh"

run() {
    peffect "grayscale"
    runim png $1 -grayscale Rec709Luminance
}
