#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "floodfill"

    hrandomcolor
    hrandom && X=$(( $HRANDOM % $WIDTH ))
    hrandom && Y=$(( $HRANDOM % $HEIGHT ))
    runim png $1 -fuzz 30% -fill $COLOR -draw "color $X,$Y floodfill"
}
