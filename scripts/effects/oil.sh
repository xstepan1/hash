#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "oil"
    runim png $1 -paint 10
}
