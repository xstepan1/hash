#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "autolevel"
    runim png $1 -auto-level
}
