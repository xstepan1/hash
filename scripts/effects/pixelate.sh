#!/bin/bash

source /scripts/preamble.sh

run() {
    peffect "pixelate"
    runim png $1 -scale 10% -scale 1000%
}
