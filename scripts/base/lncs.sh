#!/bin/bash

source /scripts/preamble.sh

run() {
    pbase "Late Night City Stories" "Nika Kunzová"
    runp5 lncs
    runim png $OUT -resize ${WIDTH}x${HEIGHT} -background black -gravity center -extent ${WIDTH}x${HEIGHT}
}
