#!/bin/bash

if [ $# -gt 1 ]; then
    echo 1>&2 "Usage: $0 [SHA256 FINGERPRINT]"
fi

if [ $# == 0 ]; then
    echo 1>&2 "WARNING: A SHA256 fingerprint has not been suplied. An all-zero fingerprint will be used!"
else
    HASH=$1
fi

source /scripts/preamble.sh
hashprint "Hash $HASH"

hrandom && LAYOUT_DEPTH=$(( ($HRANDOM % 4) + 1))
hashprint "Depth $LAYOUT_DEPTH"

runrecursive
convert $OUT /out/hash.png
