#!/bin/bash

source /scripts/preamble.sh

mkdir /testout
mkdir /testout/base
mkdir /testout/effects
mkdir /testout/layouts

for B in /scripts/base/*.sh; do
    pdebug "Testing base '$B'"
    indentpush
        source $B
        HRANDOM=0
        run
        NAME=$(basename $B .sh)
        TESTOUT="/testout/base/$NAME.png"
        cp $OUT $TESTOUT
        pdebug "Saved to '$TESTOUT'"
    indentpop
done

runscript /scripts/base/hashclock.sh
REFERENCE=$OUT

for E in /scripts/effects/*.sh; do
    pdebug "Testing effect '$E'"
    indentpush
        source $E
        HRANDOM=0
        run $REFERENCE
        NAME=$(basename $E .sh)
        TESTOUT="/testout/effects/$NAME.png"
        cp $OUT $TESTOUT
        pdebug "Saved to '$TESTOUT'"
    indentpop
done

export LAYOUT_DEPTH=0
for L in /scripts/layouts/*.sh; do
    pdebug "Testing layout '$L'"
    indentpush
        source $L
        HRANDOM=0
        run
        NAME=$(basename $L .sh)
        TESTOUT="/testout/layouts/$NAME.png"
        cp $OUT $TESTOUT
        pdebug "Saved to '$TESTOUT'"
    indentpop
done

for F in ${FAILED_SCRIPTS[*]}; do
    perror $F
done
