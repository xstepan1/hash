param(
    [Parameter(Position=0, Mandatory=$true)]
    [string] $reference,
    [Parameter(Position=1, Mandatory=$true)]
    [string] $source)

$reference = (Get-Item $reference)
$source = (Get-Item $source)
Write-Host $reference $source
$kinds = Get-ChildItem -Directory $reference

$ui = (Get-Host).UI.RawUI
foreach ($kind in $kinds) {
    $ui.ForegroundColor = 'White'
    Write-Host "Checking $($kind.Name):"
    $tests = Get-ChildItem -File $kind
    foreach ($test in $tests) {
        $other = Get-Item (Join-Path $source $kind.Name $test.Name)
        $hashRef = (Get-FileHash -Path $test -Algorithm SHA256).Hash
        $hashSrc = (Get-FileHash -Path $other -Algorithm SHA256).Hash
        if ($hashRef -eq $hashSrc) {
            $ui.ForegroundColor = 'Green'
            $name = $test.Name
            Write-Host "     OK $name"
        }
        else {
            $ui.ForegroundColor = 'Red'
            $name = $test.Name
            Write-Host "    NOK $name"
        }
    }
}
$ui.ForegroundColor = 'White'
