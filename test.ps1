$dockerTag = "hash"
$container = (docker create -it --network none -e DEBUG=true -e WIDTH=1920 -e HEIGHT=1080 "$dockerTag" /scripts/testout.sh)
docker start -ai $container
if (Test-Path testout) {
    Remove-Item -r testout
}
docker cp ${container}:/testout testout
docker stop $container | Out-Null
docker rm $container | Out-Null
