user_pref("browser.download.folderList", 2);
user_pref("browser.download.dir", "/tmp");
user_pref("browser.download.lastDir", "/tmp");
user_pref("browser.download.useDownloadDir", true);
user_pref("browser.download.viewableInternally.enabledTypes", "");
user_pref("browser.helperApps.neverAsk.saveToDisk", "application/pdf;text/plain;application/text;text/xml;application/xml;image/png");
