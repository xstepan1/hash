/**
 * @name Magic Snake
 * @author Ján Kušnír <485719 at mail.muni.cz>
 **/

String timestamp;


float hueBot = 0;
float hueTop = 0;
float hueSpread = 80;

void setupHash(){
  timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-"  + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);

  
  // size(1200, 750);
  background(0);
  noFill();
  
  colorMode(HSB, 360, 100, 100);
  stars();
  hueTop = random(100, 320);
  hueBot = hueTop - hueSpread;
  stroke(hueBot, 100, 100);
  strokeCap(SQUARE);
  strokeWeight(6);
}


void stars(){
  for (int i = 0; i < 500; i++){
    fill(random(360), random(50), random(40, 100));
    noStroke();
    circle(hx(), hy(), random(5));
  }
  noFill();
}

enum orientation{UP, RIGHT, DOWN, LEFT}

class Snake{
  float x = 0;
  float y = 0;
  float lastX = -75;
  float lastY = 0;
  boolean hor = true;
  orientation curr = orientation.RIGHT;
  orientation last = null;
  
  public void move(){
    int mult = 1;
    if (left())mult = 1;
    else mult = -1;
    if( x + (y - lastY) * mult < 0 || x + (y - lastY) * mult > width - 50
    || y + (x - lastX) * mult < 0 || y + (x - lastX) * mult > height - 50) mult *= -1;
    last = curr;
    
    float tempX = x;
    float tempY = y;
    x += (y - lastY) * mult;
    y += (tempX - lastX) * mult;
 
    lastX = tempX;
    lastY = tempY;
    
    if(x - lastX == 0){
      if(y - lastY > 0){
        curr = orientation.DOWN;
      }else{
        curr = orientation.UP;
      }
    }else{
      if(x - lastX > 0){
        curr = orientation.RIGHT;
      }else{
        curr = orientation.LEFT;
      }
    }
  }
}

void drawHash(){
   Snake snake = new Snake();
   float s = min(width / 1200, height / 750);
   int moves = (int)(random(128, 512) * s);
  for (int i = 0; i < moves; i++){
    snake.move();
    
    hueTop += random(-30, 30);
    if(hueTop > 350) hueTop = 90;
    if(hueTop < 90) hueTop = 350;
    hueBot = hueTop - hueSpread;
    

    switch (snake.curr){
      case UP:
        if(snake.last == orientation.RIGHT) drawVejarFromTopLeft(snake.lastX, snake.lastY);
        else drawVejarFromTopRight(snake.lastX, snake.lastY);
        break;
      case DOWN:
        if(snake.last == orientation.RIGHT) drawVejarFromBotLeft(snake.lastX, snake.lastY);
        else drawVejarFromBotRight(snake.lastX, snake.lastY);
        break;
      case RIGHT:
        if(snake.last == orientation.UP) drawVejarFromBotRight(snake.lastX, snake.lastY);
        else drawVejarFromTopRight(snake.lastX, snake.lastY);
        break;
      case LEFT:
        if(snake.last == orientation.UP) drawVejarFromBotLeft(snake.lastX, snake.lastY);
        else drawVejarFromTopLeft(snake.lastX, snake.lastY);
        break;
    }
  }
}

boolean left(){
  float temp = random(2);
  if(temp > 1) return true;
  else return false;
}

void drawVejarBase(float x, float y, float start, float stop){
float size = 0;
  while(size < 150){
    arc(x, y, size, size, start, stop);
    size += 25;
    stroke(random(hueBot, hueTop), 100, 100);
  }
}

void drawVejarFromTopLeft(float x, float  y){
  drawVejarBase(x, y, 0, HALF_PI);
}

void drawVejarFromTopRight(float x, float y){
  drawVejarBase(x + 75, y, HALF_PI, PI);
}

void drawVejarFromBotLeft(float x, float y){
  drawVejarBase(x, y + 75, PI + HALF_PI, TWO_PI);
}

void drawVejarFromBotRight(float x, float y){
  drawVejarBase(x + 75, y + 75, PI, PI + HALF_PI);
}


void keyPressed() {
  // if 's' key is pressed, save the current frame 
  if (key == 's' || key == 'S') saveFrame("IMG-" + timestamp + "-####.png");
}
