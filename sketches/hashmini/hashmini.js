/**
 * @name Hashmini
 * @author Adam Štěpánek <469242 at mail.muni.cz>
 **/

// @ts-check

function drawPolygonishLine(x, y, vertexCount, length = 128, freq = 2) {
    var relX = x - width / 2;
    var relY = y - height / 2;
    var r = sqrt((relX) * (relX) + (relY) * (relY));
    var baseAngle = TWO_PI / vertexCount;
    if (freq >= 0) {
        length = sin(freq * r / width) * length;
    }
    var angle = atan2(relY, relX) + PI + baseAngle / 2;
    var side = floor(angle / baseAngle);
    var sideAngle = side * baseAngle;
    var vecX = -sin(sideAngle) * length / 2;
    var vecY = cos(sideAngle) * length / 2;

    line(x - vecX, y - vecY, x + vecX, y + vecY);
}

function drawPolygonish(vertexCount = 0, hue = 0, scale = 42, length = 128, freq = 2) {
    for (var i = 0; i < 5; ++i) {
        var x = int(random(scale)) * width / scale;
        var y = int(random(scale)) * height / scale;

        stroke(random(hue, hue + 16) % 360, 80, random(60, 100));
        drawPolygonishLine(x, y, vertexCount, length, freq);
        drawPolygonishLine(y, x, vertexCount, length, freq);

        drawPolygonishLine(width - x, height - y, vertexCount, length, freq);
        drawPolygonishLine(height - y, width - x, vertexCount, length, freq);
    }
}

function setup() {
    createHashCanvas();
    background(32);
    colorMode(HSB, 360, 100, 100, 100);

    strokeWeight(12);
    let vertexCount = randint(3, 8);
    let hue = randint(0, 360);
    let scale = randint(16, 256);
    let length = randint(64, 192);
    let freq = randint(4, 12);

    let iterations = randint(256, 512);
    for (var i = 0; i < iterations; ++i) {
        noStroke();
        fill(0, 5);
        rect(0, 0, width, height);
        drawPolygonish(vertexCount, hue, scale, length, freq);
        drawPolygonish(vertexCount, hue, scale, length, freq);
    }
    saveHashCanvas("hashmini");
}
