/**
 * @name Tree Clock
 * @author Eva Kuhejdová <485343 at mail.muni.cz>
 **/

/**

Eva Kuhejodvá

Creative Constraints
Pracuj s prvočísly. (nevyuzito)
Pracuj jen se dvěmi barvami. (black & white)
Nech to na náhodě. (random funkce jsou skoro vsude)


How to read:
- day between 6 and 18 hours (black on white)
- night between 18 and 6 (white on black)
- 1 tree = 1 hour
- bigger star/snowflake = 1 minute
- smaller star/snowflake = 1 second

press T or t to show timestamp
press S or s to save image

**/


String timestamp;
float theta, theta_r;   
int rl, rr;
boolean night = false;
int[] star_r_w = new int[60];
int[] star_r_h = new int[60];

int[] star_r_w_s = new int[60];
int[] star_r_h_s = new int[60];
PShape[] list_of_trees = new PShape[12];
boolean show_time = false;

PFont f;  

void setupHash() {
  // size(1800, 1000);

  f = createFont("Liberation Sans", 16, true);
  
  for (int i = 0; i < 60; i++){
    star_r_h[i] = int(random(10, height-250));
    star_r_h_s[i] = int(random(10, height-250));
  }
  
  for (int i = 0; i < 60; i++){
    star_r_w[i] = int(random(10, width-10));
    star_r_w_s[i] = int(random(10, width-10));
  }
  
  //for (int i = 0; i < 12; i++){
  //  list_of_trees[i] = draw_tree(i+1);
  //} 
}


void drawHash() {
  timestamp = nf(hhour(), 2) + ":" + nf(hminute(), 2) + ":" + nf(hsecond(), 2);
  
  int hour_ = hhour();
  
  // check if day or night
  if (hour_ < 6 || hour_ >= 18){
    night = true;
  }
  
  if (hour_ > 12){
    hour_ -= 12;
  }
  
  if (night){
    background(0);
    frameRate(5);
    stroke(255);
  } else {
    background(255);
    frameRate(5);
    stroke(0);
  }
  
  // show timestamp in picture
  if (show_time){
    textFont(f,16);
    text(timestamp,10,20);
  }
  
  // draw trees for hours
  for (int i = 1; i <= hour_; i++){
    tree(i);
  }
  noFill();
  stars();
  stars_s();
}


// star represents 1 minute
void stars(){
  strokeWeight(1);
  if (night){
    fill(255);
  }
  for (int i = 0; i < hminute(); i++){
    circle(star_r_w[i], star_r_h[i], 8);
  }
}


// star_s represents 1 second
void stars_s(){
  strokeWeight(1);
  if (night){
    fill(255);
  } else {
    fill(0);
  }
  for (int i = 0; i < hsecond(); i++){
    circle(star_r_w_s[i], star_r_h_s[i], 2);
  }
}

// tree represents 1 hour
void tree(int number){
  
  //if(!night){
  //  strokeWeight(1);
  //} else {
    strokeWeight(0);
  //}
    rr = int(random(50, 120));
    pushMatrix();
    translate((width-50)/12*number,height);
    // Draw a line 120 pixels
    line(0,0,0,-rr);
    // Move to the end of that line
    translate(0,-rr);
    // Start the recursive branching!
    branch(rr);
    popMatrix();
}

// trying to save tree as pshape object

//PShape tree;

//PShape draw_tree(int number){
//  tree = createShape();
//    tree.beginShape();
    
//    strokeWeight(0);

//    rr = int(random(50, 120));
//    pushMatrix();
//    translate((width-50)/12*number,height);
//    // Draw a line 120 pixels
//    line(0,0,0,-rr);
//    // Move to the end of that line
//    translate(0,-rr);
//    // Start the recursive branching!
//    branch(rr);
//    popMatrix();
    
//    tree.endShape();
//  return tree;
//}

void branch(float h) {

  h *= 0.66;

  float random_h = int(random(10, 20));
  if (h > random_h) {
      theta_r = random(PI/12, PI/8);
      pushMatrix();
      rotate(theta_r);
      line(0, 0, 0, -h);
      translate(0, -h+10);
      branch(h);
      popMatrix();
      
      
      rl = int(random(1,4));
      if (rl == 2){
        pushMatrix();
        rotate(theta_r/2);
        line(0, 0, 0, -h);
        translate(0, -h+10);
        branch(h);
        popMatrix();  
      }
      
      rl = int(random(1,4));
      if (rl == 2){
        pushMatrix();
        rotate(-theta_r/2);
        line(0, 0, 0, -h);
        translate(0, -h);
        branch(h);
        popMatrix();  
      }
      
      rl = int(random(1,4));
      if (rl == 2){
        pushMatrix();
        rotate(theta_r*3);
        line(0, 0, 0, -h);
        translate(0, -h);
        branch(h);
        popMatrix();  
      }
      
      
      rl = int(random(1,4));
      if (rl == 2){
        pushMatrix();
        rotate(-theta_r*3);
        line(0, 0, 0, -h);
        translate(0, -h);
        branch(h);
        popMatrix();  
      }
      
      
      pushMatrix();
      rotate(-theta_r);
      line(0, 0, 0, -h);
      translate(0, -h);
      branch(h);
      popMatrix();
  }
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("Eva_Kuhejdova_tree_clock-" + timestamp + "-####.png");
  if (key == 't' || key == 'T') show_time = !show_time;
}
