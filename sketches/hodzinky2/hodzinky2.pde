/**
 * @name Hodzinky 2
 * @author Julka Gonová <456129 at mail.muni.cz>
 **/

//Julka Gonova
//No creative constrain
//3-phase-day focus watch


import processing.svg.*;
float x1, y1, x2, y2;

float cenX;
float cenY;
float radCif = 300;

int minute;
int hour;
float timeOffset = -PI/2;

color gray = #BFB0B0;
color sun = #ffdf3d;
color black = 33;
color night = black;
color morning = #A9ADC7;
color afternoon = #943E38;
color white = #FFFFFF;

float minStartX = 0;
float minStartY = 0;
float minEndX = 0;
float minEndY = 0;

void setupHash() {
  // size(1000, 1000);
  cenX = 0;
  cenY = 0;
  translate(width / 2, height / 2);
  scale(min(width / 500, height / 500));
  background(20);
  String timestamp = year() + "-" + minute() + "-" + millis();
  String filename = "watch" + timestamp + ".svg";
  //beginRecord(SVG, filename);

  hour = hhour();
  minute = hminute();
}

float hourAngle(int hour) {
  if (hour < 6) {
    hour = 23 + hour;
  }
  return radians((hour-6) % 8 * 40);
}

void drawHash() {
  frameRate(1);
  noStroke();
  if (hour >= 6 && hour < 14) {
    fill(morning);
  } else if (hour >= 14 && hour < 22) {
    fill(afternoon);
  } else {
    fill(night);
  }
  ellipse(cenX, cenY, radCif+1, radCif+1);
  
  minStartX = cenX + radCif * cos(timeOffset - radians(20));
  minStartY = cenY + radCif * sin(timeOffset - radians(20));
  
  minEndX = cenX + radCif * cos(timeOffset + radians(20));
  minEndY = cenY + radCif * sin(timeOffset + radians(20));
  fill(white);
  arc(cenX, cenY, radCif+1, radCif+1, timeOffset - radians(20), timeOffset + radians(20));
  fill(black);
  stroke(black);
  arc(cenX, cenY, radCif/60 * minute, radCif/60 * minute, timeOffset - radians(20), timeOffset + radians(21));
  fill(white);
  noStroke();
  arc(cenX, cenY, radCif+1, radCif+1, timeOffset + radians(20), timeOffset + radians(20) + hourAngle(hour));
  
  //endRecord();
  
}
