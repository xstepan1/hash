/**
 * @name Geometric Pattern
 * @author Hana Tokárová <484176 at mail.muni.cz>
 **/

int i = 25;
float offset = 50;
float rotator = 0;
float sideBase;

void setupHash() {
	offset = random(25, 75) * max(width / 1000, height / 1000);
	i = (int)random(20, 30);
	// size(1000, 1000);
	sideBase = max(width, height) * 4 / 3.0;
	background(0, 0, 0);
	rectMode(CENTER);
	noFill();
	// frameRate(1);
}

void drawHash() {
	background(0, 0, 0);
	
	rotator = random(PI / 4, TWO_PI);
	square_spiral(i, sideBase);
}

void square_spiral(int index, float side) {
	if (index == 1) {
		return;
	}
	
	pushMatrix();
	translate(width / 2, height / 2);
	rotate(radians(rotator * index));
	strokeWeight(index * 10);
	colorMode(HSB, 360, 100, 100, 100);
	float seed = map(hx(), 0, width, 0, 360);
	rect(0, 0, side, side);
	stroke((seed + index * 5) % 360, 90, 100, index * 2);
	popMatrix();
	
	square_spiral(index - 1, side - offset);
}

void keyPressed() {
	if (key == 's') saveFrame("Tokarova_Hana_geometric_pattern_##.png");
}
