/**
 * @name Mood
 * @author Juraj Ulbrich <445367 at mail.muni.cz>
 **/

float backgroundClr, batteryClr, energy;


void setupHash() {
  // size(300, 400);
  backgroundClr = 33;
  
  background(backgroundClr);
  
  frameRate(30);
  colorMode(HSB, 360, 100, 100, 100);
   
  batteryClr = 45;
  energy = random(0, 100) + 1;

}


void drawHash() { 
    
  background(backgroundClr, 20);
  fill(backgroundClr);
  stroke(100);
  strokeWeight(5);
  
  translate(width / 2, height / 2);
  scale(min(width / 300, height / 400));
  translate(-150, -200);
  rect(50,50,200,300);
  noStroke();
  
  fill(batteryClr,100,100,100);
  quad(60, 340 - (240 * (energy/100)), 240, 340 - (240 * (energy/100)), 240, 340, 60, 340);
  energy -=1;
  batteryClr -= 10;
  if (energy < 0) energy = 50; batteryClr = 45; 

}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("juraj_ulbrich_25min-####.png");
}
