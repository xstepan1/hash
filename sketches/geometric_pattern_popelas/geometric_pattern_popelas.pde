/**
 * @name Geometric Pattern
 * @author Jano Popeláš <janpopelas at mail.muni.cz>
 **/

// import processing.pdf.*;
// import processing.svg.*;

/*
Author: Jan Popelas

Controls:
1 - Hexagon  mode
2 - Triangle mode
3 - Square   mode
4 - Hexagon  mode with solid colors
5 - Triangle mode with solid colors
6 - Square   mode with solid colors

Arrows:
    DOWN/UP    - make the decrement of shapes smaller/larger
    LEFT/RIGHT - remove/add steps from shape drawing

S - Saves current frame as SVG file
D - Saves current frame as PDF file
F - Saves current frame as PNG file
A - Saves current frame as all of the above

P - cycle through all of the pallettes
*/

int currentMode = 1;
int solidPattern = 1;

int hexSteps = 12;
int triSteps = 6;
int rectSteps = 6;

boolean saveSVG = false;
boolean savePDF = false;

int shapeSize = 50;
float hexDecrement = 12;
float triDecrement = 6;
float rectDecrement = 6;

String pngPrefix = "shapes-twisted-####.png";
String svgPrefix = "shapes-twisted-svg-####.svg";
String pdfPrefix = "shapes-twisted-pdf-####.pdf";

int currentPallette = 0;
color pallettes[][] = {
    {#FF00C1, #00B8FF},
    {#F4F1BB, #FFA5AB},
    {#143109, #AAAE7F},
    {#FF0000, #0000FF},
    {#ED6A5A, #F4F1BB},
    {#E3170A, #A9E5BB},
    {#1F2041, #4B3F72}
};

void setupHash() {
    // size(750, 750);
    background(0);
    currentMode = ((int)random(0, 65536) % 3) + 1;
    hexDecrement = random(6, 18);
    triDecrement = random(3, 9);
    rectDecrement = random(3, 9);
    hexSteps = (int)random(6, 18);
    triSteps = (int)random(3, 9);
    rectSteps = (int)random(3, 9);
    shapeSize = (int)random(25, 75);
    currentPallette = (int)random(0, 7);

}

// This function drawHexagon was borrowed and slightly modified from openPorcessing.org
// Origin: https://www.openprocessing.org/sketch/117535/
/*
Function draws hexagon based on its center and radius
params:
    x, y - coordinates of the middle of the hexagon
    radius - radius of the hexagon
*/
void drawHexagon(float x, float y, float radius) {
  pushMatrix();
    translate(x, y);
    rotate(radians(90));
    beginShape();
        for (int i = 0; i < 6; i++) {
            pushMatrix();
            float angle = PI*i/3;
            vertex(cos(angle) * radius, sin(angle) * radius);
            popMatrix();
        }
    endShape(CLOSE);
  popMatrix();
}

/*
Draws special hexagon shape with more hexagons in the middle with interpolated colors in the middle
params:
    x, y - coordinates of the middle of the hexagon
    size - radius of the largest hexagon
    c1 - beginning color
    c2 - ending color
*/
void drawHexShapeInterpolated(float x, float y, float size, color c1, color c2) {
    float t = 0;
    float inc = 1f / float(hexSteps - 1);
    for (int i = 0; i < hexSteps; i++) {
        stroke(lerpColor(c1, c2, t));
        t += inc;
        pushMatrix();
            translate(x, y);
            rotate(radians(11 * i));
            drawHexagon(0, 0, size - (size / hexDecrement) * i);
        popMatrix();
    }
}

/*
Tiles the whole screen with special HexShape by using function drawHexShapeInterpolated
params:
    desiredPallette - index of desired pallette to be drawn out from global variable 'pallettes'
*/
void drawHexTiles(int desiredPallette) {
    int vertCount = height / shapeSize;
    int horCount = width / shapeSize;
    float hexWidth = sqrt(3) * shapeSize;
    float hexHeight = 3 * (shapeSize / 2);
    for (int i = 0; i < vertCount; i++) {
        float addon = i % 2 == 0 ? 0 : hexWidth / 2;
        for (int j = 0; j < horCount + 1; j++) {
            strokeWeight(1);
            stroke(255, 0, 0);
            drawHexShapeInterpolated(addon + j * hexWidth, i * hexHeight, shapeSize, pallettes[desiredPallette][0], pallettes[desiredPallette][solidPattern]);
        }
    }
}

// This function drawTriangle was as well borrowed, from processing forum.
// Source: https://processing.org/discourse/beta/num_1262805360.html
// Note: You might be asking yourself "Why didn't he just use triangle()?" and
// the answer is simple. I needed a function, that would draw equilateral triangle
// based on its' center and radius, and I found this one. Cheers
/*
Function draws triangle based on its middle points and its radius
params:
    x, y - coordinates of the middle of the triangle
    radius - radius of the triangle to be drawn out
*/
void drawTriangle(float x, float y, float radius) {
    beginShape();
    for( float deg = 0; deg < 360; deg += 120)
    {
        float angle = deg * PI / 180;
        float nextx = cos(angle) * radius;
        float nexty = sin(angle) * radius;
        vertex(nextx, nexty);
    }
    endShape(CLOSE);
}

/*
Draws special triangle shape with many smaller triangles inside of it
params:
    x, y - coordinates of the middle of the special triangle shape
    size - radius of the largest triangle
    angle - rotation of the initial triangle
    c1 - beginning color
    c2 - ending color
*/
void drawTriShapeInterpolated(float x, float y, float size, float angle, color c1, color c2) {
    float t = 0;
    float inc = 1f / float(triSteps - 1);
    for (int i = 0; i < triSteps; i++) {
        stroke(lerpColor(c1, c2, t));
        t += inc;
        pushMatrix();
            translate(x, y);
            rotate(radians(30 + angle));
            rotate(radians(6 * i));
            drawTriangle(0, 0, size - (size / triDecrement) * i);
        popMatrix();
    }
}

/*
Tiles the whole screen with special triangle shape by using function drawTriShapeInterpolated
params:
    desiredPallette - index of desired pallette to be drawn out from global variable 'pallettes'
*/
void drawTriTiles(int desiredPallette) {
    float triangleSide = sin(radians(60)) * shapeSize;
    int vertCount = height / (shapeSize * 3 / 2) + 2;
    int horCount = width / int(triangleSide) + 2;
    for (int i = 0; i < vertCount; i++) {
        for (int j = 0; j < horCount; j++) {
            float angle = j % 2 == 0 ? 0 : 180;
            float addon = j % 2 == 0 ? 0 : - shapeSize / 2;
            drawTriShapeInterpolated(j * triangleSide, addon + i * shapeSize * 3 / 2, shapeSize, angle,
                pallettes[desiredPallette][0], pallettes[desiredPallette][solidPattern]);
        }
    }
}

/*
Draws rectangle based on its middle points and radius
params:
    x, y - coordinates of the middle point
    radius - radius of the rectangle
*/
void drawRectangle(float x, float y, float radius) {
    rectMode(CENTER);
    rect(x, y, radius, radius);
}

/*
Draws special rectangle shape with smaller rectangles in the middle of it
params:
    x, y - coordinates of the middle point
    size - radius of the largest rectangle
    c1 - beginning color
    c2 - ending color
*/
void drawRectShapeInterpolated(float x, float y, float size, color c1, color c2) {
    float t = 0;
    float inc = 1f / float(rectSteps - 1);
    for (int i = 0; i < rectSteps; i++) {
        stroke(lerpColor(c1, c2, t));
        t += inc;
        pushMatrix();
            translate(x, y);
            rotate(radians(10 * i));
            drawRectangle(0, 0, size - (size / rectDecrement) * i);
        popMatrix();
    }
}

/*
Tiles the screen with special rectangle shape by using function drawRectShapeInterpolated
params:
    desiredPallette - index of desired pallette to be drawn out from global variable 'pallettes'
*/
void drawRectTiles(int desiredPallette) {
    int vertCount = height / shapeSize + 1;
    int horCount = width / shapeSize + 2;
    for (int i = 0; i < vertCount; i++) {
        float addon = i % 2 == 0 ? 0 : - shapeSize / 2;
        for (int j = 0; j < horCount; j++) {
            drawRectShapeInterpolated(addon + j * shapeSize, i * shapeSize, shapeSize,
                pallettes[desiredPallette][0], pallettes[desiredPallette][solidPattern]);
        }
    }
}

void drawHash() {
    // if (savePDF) {
    //     beginRecord(PDF, pdfPrefix);
    // }
    // if (saveSVG) {
    //     beginRecord(SVG, svgPrefix);
    // }
    noFill();
    strokeWeight(1);
    background(0, 0, 0);
    switch (currentMode) {
        case 1:
            drawHexTiles(currentPallette);
            break;
        case 2:
            drawTriTiles(currentPallette);
            break;
        case 3:
            drawRectTiles(currentPallette);
            break;
    }
    // if (savePDF) {
    //     endRecord();
    //     savePDF = false;
    // }
    // if (saveSVG) {
    //     endRecord();
    //     saveSVG = false;
    // }
}

void keyPressed() {
    if (key == CODED) { // Arrow keys
        if (keyCode == UP) {
            switch (currentMode) {
                case 1:
                    hexDecrement+=0.2;
                    break;
                case 2:
                    triDecrement+=0.2;
                    break;
                case 3:
                    rectDecrement+=0.2;
                    break;
            }
        }
        if (keyCode == DOWN) {
            switch (currentMode) {
                case 1:
                    hexDecrement-=0.2;
                    break;
                case 2:
                    triDecrement-=0.2;
                    break;
                case 3:
                    rectDecrement-=0.2;
                    break;
            }
        }
        if (keyCode == LEFT) {
            switch (currentMode) {
                case 1:
                    hexSteps--;
                    hexSteps = max(1, hexSteps);
                    break;
                case 2:
                    triSteps--;
                    triSteps = max(1, triSteps);
                    break;
                case 3:
                    rectSteps--;
                    rectSteps = max(1, rectSteps);
                    break;
            }
        }
        if (keyCode == RIGHT) {
            switch (currentMode) {
                case 1:
                    hexSteps++;
                    break;
                case 2:
                    triSteps++;
                    break;
                case 3:
                    rectSteps++;
                    break;
            }
        }
    }
    switch (key) {
        case '1':
        case '2':
        case '3':
            currentMode = key - '0';
            solidPattern = 1;
            break;
        case '4':
        case '5':
        case '6':
            currentMode = key - '3';
            solidPattern = 0;
            break;
        case 'a':
        case 'A':
            savePDF = true;
            saveSVG = true;
            saveFrame(pngPrefix);
            break;
        case 'd':
        case 'D':
            savePDF = true;
            break;
        case 's':
        case 'S':
            saveSVG = true;
            break;
        case 'f':
        case 'F':
            saveFrame(pngPrefix);
            break;
        case 'p':
        case 'P':
            currentPallette++;
            currentPallette %= pallettes.length;
            break;
        default:
            break;
    }
}
