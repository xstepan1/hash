/**
 * @name Lighthouse Clock
 * @author Vojta Juránek <juranek.v at mail.muni.cz>
 **/

int size = 500;
Lighthouse lght;
float time;
PVector [] stars;


float shadowAngle = radians(155);
float lightIntensity = 1.0f;
float shadowOpacity = 90.0f;
float shadowWeight = 40.0f;
float shadowPos = -150.0f;
float brightness;
float sky = -100;


String timestamp;


void mapAllTheThings() {
    if (time < 14) {
        shadowPos = map(time, 0, 14, -150, 870);
      
        if (time < 7) {
            lightIntensity = map(time, 0, 7, 1, 40);
            brightness = map(time, 0, 7, 0, -80);

        } else {
            lightIntensity = map(time, 7, 14, 40, 1);
            brightness = map(time, 7, 14, -80, 0);
        }
    } else {
        shadowAngle = map(time, 14, 24, radians(150), radians(30));

        if (time < 20) {
            shadowOpacity = map(time, 14, 20, 60, 140);
            shadowWeight = map(time, 14, 20, 35, 55);
            sky = map(time, 14, 20, -60, 0);
        } else {
            shadowOpacity = map(time, 20, 24, 140, 60);
            shadowWeight = map(time, 20, 24, 55, 35);
            sky = map(time, 20, 24, 0, -60);
        }
    }
}

void initStars(){
    float s = min(width / 720, height / 720);
   for (int i =0; i<70;i++){
   stars[i] = new PVector((int)random(-width / 2 / s,width / 2 / s),(int)random(-height / 2 / s,height / 2 / s),(int)random(0,10));
  }
}

void drawStars(){
    stroke(#FFFF66);
    fill(#FFFF66);
    strokeWeight(1);
    for (int i =0; i<70;i++){
      circle(stars[i].x,stars[i].y,stars[i].z);
  }
}

void setupHash() {
    frameRate(1);
    // size(720, 720);
    timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-" + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);

    translate(width / 2, height / 2);
    scale(min(width / 720, height / 720));
    lght = new Lighthouse(360, 320);
    stars = new PVector[70];
    initStars();
  
}

void drawHash() {
    float currentTime = hhour() + (float) hminute() / 60 + (float) hsecond() / 3600;
    time = currentTime + 8;
    if (time >= 24.0f) time = time - 24.0f;
    //time = map (mouseX,0,width,0,24);
    colorMode(HSB, 360, 100, 100);
    background(222, 66, 60 + sky);
    
    if (time < 14.0f) {
      drawStars();
    }
    
    
    pushMatrix();
    translate(-360, -360);

    fill(56, 83, 100 + brightness);
    stroke(56, 83, 100 + brightness);
    strokeWeight(1);
    circle(720 / 2, 720 / 2, size);
    colorMode(RGB);
  
    mapAllTheThings();
    
    if (time > 14.0f) {
        float x = cos(shadowAngle) * 170 + 360;
        float y = sin(shadowAngle) * 120 + 320;

        stroke(0, 0, 0, shadowOpacity);
        strokeWeight(shadowWeight);
        line(360, 355, x, y);
    }

    lght.plot();

    if (time < 14.0f) {
        fill(0);
        stroke(0);
        circle(shadowPos, 720 / 2, size + 4);
    }

    lght.plotLight();
    popMatrix();
}


void keyPressed() {
    if (key == 's' || key == 'S') saveFrame("IMG-" + timestamp + "-####.png");
}

class Lighthouse {
    PShape lighthouse;
    int x, y;

    Lighthouse(int x, int y) {
        this.x = x;
        this.y = y;

        lighthouse = createShape(GROUP);

        PShape base = createShape();
        base.beginShape(QUADS);
        base.stroke(240);
        base.fill(240);
        base.vertex(-20, -50);
        base.vertex(20, -50);
        base.vertex(30, 50);
        base.vertex(-30, 50);
        base.endShape();
        lighthouse.addChild(base);

        PShape stripe1 = createShape();
        stripe1.beginShape(QUADS);
        stripe1.stroke(240, 0, 0);
        stripe1.fill(240, 0, 0);
        stripe1.vertex(-28, 30);
        stripe1.vertex(28, 30);
        stripe1.vertex(30, 50);
        stripe1.vertex(-30, 50);
        stripe1.endShape();
        lighthouse.addChild(stripe1);

        PShape stripe2 = createShape();
        stripe2.beginShape(QUADS);
        stripe2.stroke(240, 0, 0);
        stripe2.fill(240, 0, 0);
        stripe2.vertex(-26, 10);
        stripe2.vertex(26, 10);
        stripe2.vertex(24, -10);
        stripe2.vertex(-24, -10);
        stripe2.endShape();
        lighthouse.addChild(stripe2);

        PShape stripe3 = createShape();
        stripe3.beginShape(QUADS);
        stripe3.stroke(240, 0, 0);
        stripe3.fill(240, 0, 0);
        stripe3.vertex(-20, -50);
        stripe3.vertex(20, -50);
        stripe3.vertex(22, -30);
        stripe3.vertex(-22, -30);
        stripe3.endShape();
        lighthouse.addChild(stripe3);

        PShape square = createShape();

        square.beginShape(QUADS);
        square.stroke(240);

        square.fill(240);
        square.vertex(-20, -92);
        square.vertex(20, -92);
        square.vertex(20, -52);
        square.vertex(-20, -52);
        square.endShape();
        lighthouse.addChild(square);

        PShape roof = createShape();
        roof.beginShape(TRIANGLES);
        roof.stroke(240, 0, 0);
        roof.fill(240, 0, 0);
        roof.vertex(-25, -94);
        roof.vertex(25, -94);
        roof.vertex(0, -120);
        roof.endShape();
        lighthouse.addChild(roof);

        lighthouse.translate(x, y);

    }

    void plot() {
        shape(lighthouse);
        strokeWeight(1);
        stroke(150);
        fill(150);
        circle(x, y - 72, 35);
    }

    void plotLight() {
        stroke(#FFFF66);
        fill(#FFFF66);
        strokeWeight(lightIntensity);
        circle(x, y - 72, 30);
        stroke(#FFFF99);

        fill(#FFFF99);
        circle(x, y - 72, 20);
    }
}
