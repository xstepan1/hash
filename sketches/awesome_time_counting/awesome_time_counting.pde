/**
* Awesome time counting
*
* @author  Jakub Kolarovksy
*
* constraints used:
* - think for 5 minutes before you start
* - oscilation (made the result a bit confusing hehe
*/

color handleColor = color(264, 4, 100);
color bgColor = color(0, 0, 0);
color numbersColor = color(0, 0, 0);

void setupHash() {
  // size(600, 400);
  colorMode(HSB, 360, 100, 100);
  background(33);
  frameRate(30);
  handleColor = color(264, 4, 100);
}

void darkColors() {
  bgColor = color(180, 41, 32);
  numbersColor = color(169, 28, 84);
}

void brightColors() {
  bgColor = color(299, 63, 72);
  numbersColor = color(272, 40, 79);
}

void drawNumbers(float len, float step) {
  fill(bgColor);
  noStroke();
  arc(0, 0, len, len, 0, PI);
  stroke(numbersColor);
  
  float startLen = len / 3;
  float endLen = len / 2;
  for (float i = 0; i < step +1; i++) {
    float inDegrees = map(i, 0, step, 0, 180);
    float startX = startLen * cos(radians(inDegrees));
    float startY = startLen * sin(radians(inDegrees)); 
    float endX = endLen * cos(radians(inDegrees));
    float endY = endLen * sin(radians(inDegrees));
    line(startX, startY, endX, endY);
  }
}

float[] drawArrow(float val, float len, float range, float spacing) {  
  float inDegrees = 0;  
  float midPoint = range / 2;
  
  if (val < midPoint) {
    darkColors();
    inDegrees = map(val, 0, midPoint - 1, -90, 90 - (180 / midPoint));
  } else {
    brightColors();
    inDegrees = map(val, midPoint, range - 1, 90, -90 + (180 / midPoint));
  }
  
  drawNumbers(len * 2, spacing);
  float x = -len * sin(radians(inDegrees));
  float y = len * cos(radians(inDegrees));
  stroke(handleColor);
  line(0, 0, x, y);
  return new float[] {x, y, inDegrees};
}

void drawHash() {
  background(33);
  
  strokeWeight(10);
  translate(width/2, 20);
  
  float s = min(width / 600, height / 400);
  float[] hourTranslation = drawArrow(hhour(), 150 * s, 24, 6);
  translate(hourTranslation[0], hourTranslation[1]);
  rotate(radians(hourTranslation[2]));
  
  float[] minuteTranslation = drawArrow(hminute(), 100 * s, 60, 6);
  translate(minuteTranslation[0], minuteTranslation[1]);
  rotate(radians(minuteTranslation[2]));
  
  drawArrow(hsecond(), 50 * s, 60, 6);
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("jakub_kolarovsky_clock_####.png");
}
