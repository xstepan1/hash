/**
 * @name Labyrinth
 * @author Juraj Ulbrich <445367 at mail.muni.cz>
 **/

String timestamp;

/*

0-6 = number of max lines in a square
Space = enable/disable diagonal lines
7 = Enable paths (works best without diagonal lines)
8 = enable dots
s/S = Save Image

*/



// pokus 2
int spaceing = 50;
int linesmax = 2;
int lines;
boolean crossed = false;
boolean allowDiagonal = false;
boolean path = false;
boolean points = false;


void setupHash() {
  
  timestamp = hyear() + nf(hmonth(),2) + nf(hday(),2) + '-' + nf(hhour(),2) + nf(hminute(),2) + nf(hsecond(),2);
  println(timestamp);

  colorMode (HSB, 360, 100, 100, 100);

  // size(1000, 1000);
  frameRate(1);
  
  background(0, 100, 10, 10);
  
  stroke(0, 0, 100, 100);
  strokeWeight(3);
  
  fill(100, 0, 100, 100);
}

void drawHash () {

  

  background(0, 100, 10, 10);

 
  for (int x = 0; x <= width; x += spaceing) {
    for (int y = 0; y <= height; y += spaceing) {
      lines = 0;
      crossed = false;
      
      if(random(2) <= 1 && lines < linesmax && y <= 0) { //<>//
          line(x, y, x + spaceing, y);
          //lines++;
      }
      
      if(random(2) <= 1 && lines < linesmax && x <= 0) {
          line(x, y, x, y + spaceing);
          //lines++;
      }
      
      if(random(2) <= 1 && lines < linesmax) {
          line(x + spaceing, y, x + spaceing, y + spaceing);
          lines++;
      } else if (path) {
          stroke(0, 100, 100, 100);
          line(x + (spaceing/2) , y + (spaceing/2) , x + spaceing + (spaceing/2), y +  (spaceing/2));
          stroke(0, 0, 100, 100);
      }
      
      if(random(2) <= 1 && lines < linesmax) {
          line(x, y + spaceing, x + spaceing, y + spaceing);
          lines++;
      } else if (path) {
          stroke(0, 100, 100, 100);
          line(x + (spaceing/2) , y + (spaceing/2) , x + (spaceing/2), y + spaceing + (spaceing/2));
          stroke(0, 0, 100, 100);
      }
      
      if(random(2) <= 1 && lines < linesmax && allowDiagonal) {
          line(x, y, x + spaceing, y + spaceing);
          lines++;
          crossed = true;
      }
      
      if(random(2) <= 1 && lines < linesmax && allowDiagonal) {
          line(x, y + spaceing, x + spaceing, y);
          lines++;
          crossed = true;
      }
      
      if(!crossed && points) {
        ellipse((x+(spaceing/2)), (y+(spaceing/2)), spaceing / 5, spaceing / 5);
      }
    }
  }
  
}


void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("juraj-ulbrich-labyrinth-" + timestamp + "-####.png");
  if (key == '1') linesmax = 1;
  if (key == '2') linesmax = 2;
  if (key == '3') linesmax = 3;
  if (key == '4') linesmax = 4;
  if (key == '5') linesmax = 5;
  if (key == '6') linesmax = 6;
  if (key == '0') linesmax = 0;
  if (key == ' ') allowDiagonal = !allowDiagonal;
  if (key == '7') path = !path;
  if (key == '8') points = !points;
}

void mouseClicked() {
  spaceing = mouseY;
  strokeWeight((mouseX/100));
}
