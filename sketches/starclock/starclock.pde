/**
 * @name Squares
 * @author Tamara Babálová <485550 at mail.muni.cz>
 **/

float center_x, center_y;
float radius;
float secondsRadius, minutesRadius, hoursRadius;
int scr,scg,scb;
color bgColor;
String timestamp;

void setupHash() {
  // size(1200, 1200);

  background(33);
  
  timestamp = year() + "-" + minute() + "-" + millis();

  center_x = width/2;
  center_y = height/2;

  radius = height/4;
  
  secondsRadius = radius * 0.72 + 70;
  minutesRadius = radius * 0.60 + 50;
  hoursRadius = radius * 0.50 + 30;
  
  scr = 255;
  scg = 255;
  scb = 204;
  bgColor = #7799d6;
}

void drawHash() {
  fill(#ffffff, 13);
  rect(0, 0, width, height);
  strokeWeight(1);
  
  //noise bg
  float xstart = random(10);
  float xnoise = xstart;
  float ynoise = random(10);
  for(int y = 0; y <= height; y +=10){
    ynoise += 0.2;
    xnoise = xstart;
    for(int x = 0; x <= width; x+=10){
      xnoise += 0.2;
      drawPoint(x,y,noise(xnoise,ynoise));
    }
  }
  
  //outer hour stars
  for(int i = 0; i <= 12; i++){
    float posx=450*sin(TWO_PI/12.0*i)+center_x;
    float posy=450*cos(TWO_PI/12.0*i)+center_y;
    stroke(#ffffff);
    //random blinking
    fill(scr,scg,scb,255-map(hmillis()%3000,300,random(1000,3000),300,random(100,300)));
    star(posx,posy,40,65,6);
  }
  
  //middle minute stars
  for(int i = 0; i <= 60; i++){
    float posx=320*sin(TWO_PI/60.0*i)+center_x;
    float posy=320*cos(TWO_PI/60.0*i)+center_y;
    stroke(#ffffff);
    //random blinking
    fill(scr,scg,scb,255-map(hmillis()%3000,300,random(1000,3000),300,random(100,300)));
    star(posx,posy,5,10,6);
  }
  
  stroke(#ffffff);
  noFill();
  ellipse(center_x, center_y, 2*radius, 2*radius);
  ellipse(center_x, center_y, 2.4*radius, 2.4*radius);  
  
  float s = map(hsecond(), 0, 60, 0, TWO_PI) - HALF_PI;
  float m = map(hminute() + norm(hsecond(), 0, 60), 0, 60, 0, TWO_PI) - HALF_PI; 
  float h = map(hhour() + norm(hminute(), 0, 60), 0, 24, 0, TWO_PI * 2) - HALF_PI;
  
  //hands of the clock
  stroke(255);
  strokeWeight(3);
  line(center_x, center_y, center_x + cos(s) * secondsRadius, center_y + sin(s) * secondsRadius);
  strokeWeight(6);
  line(center_x, center_y, center_x + cos(m) * minutesRadius, center_y + sin(m) * minutesRadius);
  strokeWeight(10);
  line(center_x, center_y, center_x + cos(h) * hoursRadius, center_y + sin(h) * hoursRadius);
}

void star(float x, float y, float radius1, float radius2, int npoints) {
  float angle = TWO_PI / npoints;
  float halfAngle = angle/2.0;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius2;
    float sy = y + sin(a) * radius2;
    vertex(sx, sy);
    sx = x + cos(a+halfAngle) * radius1;
    sy = y + sin(a+halfAngle) * radius1;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

void drawPoint(int x, int y, float noiseFactor){
  push();
  translate(x,y);
  rotate(noiseFactor*radians(360));
  stroke(bgColor);
  line(0,0,20,10);
  pop();
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("Star Clock-" + timestamp + "-####.png");
}
