/**
 * @name High Effort Planety
 * @author Jan "Puf" Pokorný <puff at mail.muni.cz>
 **/

String timestamp;
Boolean communismMode = false;

void setupHash() {
  // size(720, 720);
  frameRate(1);
  timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-"  + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
}

void drawHash() {
  final float TILE_SIZE = 120;
  
  final float PLANET_SIZE_MIN = 7;
  final float PLANET_SIZE_MAX = 20;
  
  final int MOON_COUNT_MAX = 3;
  final float MOON_SIZE_MIN = 2;
  final float MOON_SIZE_MAX = 7;
  final float MOON_PLANET_DISTANCE_MIN = 10;
  final float MOON_PLANET_DISTANCE_MAX = 15;
  final float MOON_MOON_DISTANCE_MIN = 20;
  final float MOON_MOON_DISTANCE_MAX = 25;
  final float MOON_TRAJECTORY_WEIGHT = 1.5;

  final float SHADOW_DISTANCE = 4;
  final float SHADOW_BRIGHTNESS = 15;
  final float SHADOW_BLUR_RADIUS = 5;
  
  PGraphics bg = createGraphics(width, height);
  PGraphics sh = createGraphics(width, height);
  PGraphics fg = createGraphics(width, height);
  
  bg.beginDraw();
  sh.beginDraw();
  fg.beginDraw();
  
  bg.ellipseMode(RADIUS);
  sh.ellipseMode(RADIUS);
  fg.ellipseMode(RADIUS);
  
  if(!communismMode) {
    bg.colorMode(HSB, 360, 100, 100, 100);
    sh.colorMode(HSB, 360, 100, 100, 100);
    fg.colorMode(HSB, 360, 100, 100, 100);
  }
  
  bg.background(0, 0, 24);
  
  for(float centerX = TILE_SIZE/2; centerX < width; centerX += TILE_SIZE) {
    for(float centerY = TILE_SIZE/2; centerY < height; centerY += TILE_SIZE) {
      // RANDOMIZE
      
      final float planetSize = random(PLANET_SIZE_MIN, PLANET_SIZE_MAX);
      final int moonCount = (int)random(0, MOON_COUNT_MAX);
      
      float moonDistance[] = new float[moonCount];
      float moonSize[] = new float[moonCount];
      float moonRotation[] = new float[moonCount];
      for(int i = 0; i < moonCount; i++) {
        if(i == 0)
          moonDistance[i] = random(planetSize + MOON_PLANET_DISTANCE_MIN, planetSize + MOON_PLANET_DISTANCE_MAX);
        else
          moonDistance[i] = random(moonDistance[i-1] + MOON_MOON_DISTANCE_MIN, moonDistance[i-1] + MOON_MOON_DISTANCE_MAX);
         
        moonSize[i] = random(MOON_SIZE_MIN, MOON_SIZE_MAX);
        moonRotation[i] = random(0, TAU);
      }
      
      // DRAW BACKGROUND
      
      bg.noStroke();
      bg.fill(200, 0, 32);
      bg.triangle(centerX-TILE_SIZE/2, centerY-TILE_SIZE/2, centerX+TILE_SIZE/2, centerY-TILE_SIZE/2, centerX-TILE_SIZE/2, centerY+TILE_SIZE/2);
      
      // DRAW SHADOWS
      
      // planet shadow
      sh.noStroke();
      sh.fill(0, 0, SHADOW_BRIGHTNESS);
      sh.circle(centerX + SHADOW_DISTANCE, centerY + SHADOW_DISTANCE, planetSize);
      
      // moons
      for(int i = 0; i < moonCount; i++) {
        // body shadow
        sh.noStroke();
        sh.fill(0, 0, SHADOW_BRIGHTNESS);
        sh.circle(centerX + sin(moonRotation[i])*moonDistance[i] + SHADOW_DISTANCE, centerY + cos(moonRotation[i])*moonDistance[i] + SHADOW_DISTANCE, moonSize[i]);
        
        // trajectory shadow
        sh.stroke(0, 0, SHADOW_BRIGHTNESS);
        sh.strokeWeight(3);
        sh.noFill();
        sh.circle(centerX + SHADOW_DISTANCE, centerY + SHADOW_DISTANCE, moonDistance[i]);
      }
      
      // DRAW OBJECTS
      
      // planet
      fg.noStroke();
      fg.fill(random(360), 60, 100);
      fg.circle(centerX, centerY, planetSize);
      
      // light half
      fg.fill(0, 0, 100, 40);
      fg.arc(centerX, centerY, planetSize, planetSize, -HALF_PI, HALF_PI);
      
      // moons
      for(int i = 0; i < moonCount; i++) {        
        // trajectory
        fg.stroke(0, 0, 80);
        fg.strokeWeight(MOON_TRAJECTORY_WEIGHT);
        fg.noFill();
        fg.circle(centerX, centerY, moonDistance[i]);
        
        // moon
        fg.noStroke();
        fg.fill(random(360), 60, 100);
        fg.circle(centerX + sin(moonRotation[i])*moonDistance[i], centerY + cos(moonRotation[i])*moonDistance[i], moonSize[i]);
      }
    }
  }
  
  sh.filter(BLUR, SHADOW_BLUR_RADIUS);
  
  bg.endDraw();
  sh.endDraw();
  fg.endDraw();
  
  image(bg, 0, 0);
  image(sh, 0, 0);
  image(fg, 0, 0);
}

void keyPressed() {
  // S - save frame
  if (key == 's') saveFrame("IMG-" + timestamp + "-####.png");
  
  // C - communism mode
  if (key == 'c') communismMode = true;
  
  // N - normal mode
  if (key == 'n') communismMode = false;
}
