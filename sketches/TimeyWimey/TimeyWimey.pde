/**
 * @name TimeyWimey
 * @author Nika Kunzová <nikkun at mail.muni.cz>
 **/

//Creative constraints:
// Kontrasty
// Urob to naruby.
// Urob to minimalisticky.

//General
int midX01, midX02, midY;
int midCircRad;

//Seconds
int maxLineLength = 120;
boolean secondsSize = true;

//Minutes
int minsIterations = 1;
int minsMaxIterations = 8;

//Hours
int hoursIterations = 0;
int hoursMaxIterations = 60;
int hdirBase = 1;

//Fake Time Changer
int minFake = 0;
int hrFake = 0;

void setupHash() {
  colorMode(HSB, 360, 100, 100, 100);
  background(0);
  
  midX01 = width/2 + 26;
  midX02 = width/2 - 26;
  midY = height/2;
  midCircRad = 200;
  
  float sec = random(0, 60);
  float res = sec / 15;
  if (res == 1 || res == 3) {
    hdirBase *= (-1);
  }

  drawHash();
  save(outDir + "/" + "TimeyWimey_" + hash + ".png");
  exit();
}

void drawHash() {
  int sec = (int)random(0, 60);
  if (sec == 0) secondsSize = !secondsSize;
  
  //middleCircles();
  
  //seconds(midX01, midY, midCircRad, 20, false);
  //seconds(midX02, midY, midCircRad, 20, true);
  
  minutes(midX01, midY, midCircRad, 20, false);
  minutes(midX02, midY, midCircRad, 20, true);
  minsIterations ++;
  if (minsIterations >= minsMaxIterations) {
    minsIterations = minsMaxIterations;
  }
  
  if (sec == 0 || sec == 30) {
    hdirBase *= (-1);
  }
  
  hours(midX01, midY, midCircRad, 20, false, 1, hdirBase);    
  hours(midX02, midY, midCircRad, 20, true, -1, hdirBase);

  noStroke();
  fill(0, 0, 0, 5);
  rect(0, 0, width, height);
}

void middleCircles() {
  noFill();
  stroke(0, 0, 100);
  strokeWeight(1);
  arc(midX01, midY, midCircRad, midCircRad, - HALF_PI - QUARTER_PI / 3, HALF_PI + QUARTER_PI / 3);
  arc(midX02, midY, midCircRad, midCircRad, 5 * QUARTER_PI / 3, PI + HALF_PI + QUARTER_PI / 3);
}

void seconds(int x, int y, int circRad, int offset, boolean opposite) {
  float sec = random(0, 60);
  if (opposite) sec = 60 - sec;
  float ratio = sec / 60;
  float angle = TWO_PI * ratio - HALF_PI;
  if ((opposite && secondsSize) || (!opposite && !secondsSize)) {
    ratio = (60 - sec) / 60;
  }
  
  float innerX = (circRad / 2 + offset) * cos(angle);
  float innerY = (circRad / 2 + offset) * sin(angle);
  
  float outerX = (circRad / 2 + offset + ratio * maxLineLength) * cos(angle);
  float outerY = (circRad / 2 + offset + ratio * maxLineLength) * sin(angle);
  
  noFill();
  stroke(0, 0, 100);
  strokeWeight(2);
  line(innerX + x, innerY + y, outerX + x, outerY + y);
}

void minutes(int x, int y, int circRad, int offset, boolean opposite) {
  float min = random(0, 60) - minFake;
  
  if (opposite) {
    min += 30;
    if (min >= 60) min -= 60;
  }
  
  float ratio = min / 60;
  float angle = TWO_PI * ratio - HALF_PI;
  
  float innerX = (circRad / 2 + offset) * cos(angle);
  float innerY = (circRad / 2 + offset) * sin(angle);
  
  int newOffset = offset + offset;
  
  noFill();
  stroke(0, 0, 100);
  strokeWeight(1);
  
  line(width/2, height/2, x + innerX, y + innerY);
  
  for (int i = 0; i < minsIterations; i++) {
    float newX = (circRad / 2 + newOffset) * cos(angle);
    float newY = (circRad / 2 + newOffset) * sin(angle);
    line(innerX + x, innerY + y, newX + x, newY + y);
    innerX = newX;
    innerY = newY;
    newOffset += newOffset;
    angle += random(-0.2, 0.2);
  }
}

void hours(int x, int y, int circRad, int offset, boolean opposite, int dir, int hdir) {
  float hr = random(0, 24) - hrFake;
  if (opposite) hr += 6;
  if (hr >= 12) hr -= 12;
  if (hr >= 12) hr -= 12;
  
  float ratio = hr / 12;
  float angle = TWO_PI * ratio - HALF_PI;
   
  float innerX = (circRad / 2) * cos(angle);
  float innerY = (circRad / 2) * sin(angle);
  
  noFill();
  stroke(0, 0, 100);
  strokeWeight(3);
  
  if (opposite) offset *= (-1);
  int endOffset = 1;
  if (opposite) endOffset *= (-1);
  
  int w = width;
  int wbase = width / 2;

  if (opposite) {
    w = 0;
    //wbase = 0;
  }
  
  beginShape();
  
  curveVertex(width/2, height/2);
  curveVertex(width/2, height/2);
  curveVertex(width/2, height/2 + offset);
  curveVertex(x, y);
  curveVertex(innerX + x, innerY + y);
  curveVertex(width/2 + endOffset * 200, height/2);
  
  int min = (int)random(0, 60) - minFake;
  float heightRatio = calculateHeightRatio();
  int cycleInvr = 1;
  
  if (min != 0) {
    
    int minCount = (min) / 15;
    
    int waveLength = width / (2 * (minCount + 1));
    int waveHeight = height / (2 * (minCount + 1));
    
    for (int i = 0; i < (minCount + 1); ++i) {
      curveVertex(wbase + dir * 100 + dir * (i + 1) * waveLength, 
                  height/2 + dir * hdir * (i + 1) * waveHeight * heightRatio * cycleInvr);
      cycleInvr *= (-1);
    }
  }
  
  
  //curveVertex(wbase + dir * 100 + width/4 , height/2 + dir * hdir * height/5 * heightRatio);
  
  curveVertex(w, height/2 + dir * hdir * height/2 * heightRatio);
  curveVertex(w, height/2 + dir * hdir * height/2 * heightRatio);
  
  endShape();
  
}

private float calculateHeightRatio() {
  int sec = (int)random(0, 60);
  
  if (sec == 45 || sec == 15) return 1;
  if (sec == 30 || sec == 0) return 0;
  
  int divid = sec / 15;
  
  float res = sec - divid * 15;
  
  //if ((sec > 30 && sec <= 45) || (sec > 0 && sec <= 15)) {
  //if(hdirBase == -1) {
  if ((sec > 15 && sec <= 30)|| (sec > 45 && sec <= 59))  {
    res = 15 - res;
  }
  
  return res/15;
}
