/**
 * @name Clock
 * @author Franta Holubec <xholubec at mail.muni.cz>
 **/

String timestamp;
float radius;
float cX;
float cY;


void setupHash() {
  // size(1920, 1080);
  frameRate(1);
  colorMode(HSB, 360, 100, 100, 100);
  background(30);
  radius = height/2-50;
  cX = width/2;
  cY = height/2;
}



void drawHash(){
  int hour = hhour();
  int minute = hminute();
  int second = hsecond();
  
  timestamp = nf(hour, 2) + "h" + nf(minute, 2) + "m" + nf(second, 2) + "s";
  background(30);
  
  float secondsFill = map(second, 0, 60, 0.01, 1);
  float minutesAngle = map(minute, 0, 60,  -PI/2, TWO_PI - PI/2);
  float hoursAngle = map(hour, 0, 12, PI, -PI);

  float hourCenterX = cX + radius/2 * sin(hoursAngle);
  float hourCenterY = cY + radius/2 * cos(hoursAngle);
  
  drawLines(cX, cY, 50, radius - 50, 5, 10, color(40));
 
  stroke(50);
  strokeWeight(20); 
  noFill(); 
  float circleCount = 4;
  for(int i = 1; i <= circleCount; i++){
    circle(cX,  cY, radius*2/circleCount*i);
  }
  circle(cX, cY, 20);
 
  drawLines(hourCenterX, hourCenterY, 50, radius/2 - 50, 5, 10, color(80));
 
  filter(BLUR, 2);
   
  stroke(#ec3c32);
  strokeWeight(10); 
  circle(cX, cY, radius*2*secondsFill);
  
  stroke(#09cbc3);
  strokeWeight(20); 
  circle(cX,  cY, radius*2 + 50);
  
  circle(hourCenterX, hourCenterY, radius-50);
  circle(hourCenterX, hourCenterY, 20);
  
  stroke(#ec3c32);
  strokeWeight(15); 
  float minuteCircleCount = 4;
  for(int i = 1; i < minuteCircleCount; i++){
    float cr = (radius-50)/minuteCircleCount*i;
    float wd = 0.12/(i/minuteCircleCount);
    arc(hourCenterX, hourCenterY, cr, cr, minutesAngle+wd - TWO_PI, minutesAngle-wd + 0);
  }
  
  filter(BLUR, 1);
}

void drawLines(float x, float y, float innerR, float outerR, float thinW, float thickW, color col){
  int lineCount=12;
  float angle=TWO_PI/(float)lineCount;
  stroke(col);
  noFill(); 
  for(int i=0;i<lineCount;i++)
  {
    if(i % 3 == 0) strokeWeight(thickW); 
    else strokeWeight(thinW); 
   
    float s = sin(angle*i);
    float c = cos(angle*i);
    float r = radius - 50;
    line(x + innerR*s , y + innerR*c, x + outerR*s, y + outerR*c);
  } 
}


void keyPressed() {
  // if 's' key is pressed, save the current frame 
  if (key == 's' || key == 'S') saveFrame("fratna_holubec_clock-" + timestamp + "-####.png");
}
