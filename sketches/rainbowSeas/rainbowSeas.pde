/**
 * @name Rainbow Seas
 * @author Vojta Juránek <juranek.v at mail.muni.cz>
 **/

int noOfEdges;
int edgeSharpness;
int strokeWeight;
int r,g,b;
int noOfVertices;

String timestamp;
boolean setFlag; //specifies if the wave shape has been set

ArrayList<Boat> boats;
PVector[] vertices;

boolean boatsActive = true;
int colorMode = 1;


void setupHash() {
  strokeWeight = 30;
  frameRate(30);
  // size(720, 720);
  strokeWeight(strokeWeight);
  
  setFlag = false;
  
  boats = new ArrayList<Boat>();
  int noOfBoats = (int)random(1,10 * min(width / 720, height / 720));
  for (int i = 0;i<noOfBoats;i++){
    boats.add (new Boat((int)random(50, width - 20), (int)random(50, height - 20), radians(random(-20,20))));
  }

  r = (int) random(0, 150);
  g = (int) random(0, 150);
  b = (int) random(0, 150);

  timestamp = year() + nf(month(), 2) + nf(day(), 2) + "-" + nf(hour(), 2) + nf(minute(), 2) + nf(second(), 2);
}



void generateLine() {
  background(255);

  int tempR = r;
  int tempG = g;
  int tempB = b;

  //this generates the geometry of the waves
  if (!setFlag) {  
    noOfVertices = noOfEdges * 2 + 1;
    vertices = new PVector[noOfVertices];
    int increment = (int) width / noOfEdges / 2;
    vertices[0] = new PVector(0, 0);
    vertices[noOfVertices - 1] = new PVector(width, 0);
    for (int i = 1; i < noOfVertices - 1; i++) {
      vertices[i] = new PVector(vertices[i - 1].x + increment, (int) random(0, edgeSharpness));
    }
    setFlag = true;
  }
  
  //this draws the waves and chooses color
  for (int j = -60; j < height + 20; j += strokeWeight - 1) { 
    if (colorMode==1){  
      stroke(tempR, tempG, tempB);
      tempR += 7;
      tempG += 7;
      tempB += 7;
    }
    if (colorMode ==2){
      stroke ((int)random(100,255),(int)random(100,255),(int)random(100,255) );
      noLoop();
    }
    for (int i = 0; i < noOfVertices - 1; i++) {
      line(vertices[i].x, vertices[i].y + j, vertices[i + 1].x, vertices[i + 1].y + j);   
    }
    
    //this draws the boat
    if (boatsActive){
      for (Boat boat : boats){
        if (boat.y > j) boat.plot();
      } 
    }
  }
}

void drawHash() {
  noOfEdges = (int) map(hx(), 0, width, 1, 6);
  edgeSharpness = (int) map(hy(), 0, height, 20, 70);
  generateLine();
  boatAnimation();
}

//this animates the boats
void boatAnimation() {
  for (Boat boat : boats){
    if (Math.abs(boat.totalRotated) >= radians(20)) 
      boat.rotation *= -1;
  
    boat.rotate( boat.rotation);
    boat.totalRotated +=  boat.rotation; 
  }
}

void mousePressed() {
   if (mouseButton == LEFT) setFlag = false;
   else {
      r = (int) random(0, 150);
      g = (int) random(0, 150);
      b = (int) random(0, 150);
   }
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("IMG-" + timestamp + "-####.png");
  if (key == 'r' || key == 'R') {
    setup();
    redraw();
  }
  if (key == 'b' || key == 'B') boatsActive = !boatsActive;
  if (key == '1' ) {
    colorMode = 1;
    loop();
    redraw();
  }
  if (key == '2' ) colorMode = 2;
}

class Boat {
  PShape boat;
  int x, y;
 
  float totalRotated;
  float rotation;

  Boat(int x, int y, float startRotation) {
    this.x = x;
    this.y = y;
    totalRotated = startRotation;
    rotation = 0.01f;
    
    boat = createShape(GROUP);
    
    strokeWeight(1);
    PShape bridge = createShape();
    bridge.beginShape(TRIANGLES);
    bridge.stroke(180);
    bridge.fill(180);
    bridge.vertex(0, -40);
    bridge.vertex( - 25, 10);
    bridge.vertex(0, 10);
    bridge.endShape();
    boat.addChild(bridge);
 
    PShape bridgeHighlight = createShape();
    bridgeHighlight.beginShape(TRIANGLES);
    bridgeHighlight.stroke(230);
    bridgeHighlight.fill(230);
    bridgeHighlight.vertex(0, -40);
    bridgeHighlight.vertex(0, 10);
    bridgeHighlight.vertex(25, 10);
    bridgeHighlight.endShape();
    boat.addChild(bridgeHighlight);

    PShape hull = createShape();
    hull.beginShape(QUAD);
    hull.stroke(150);
    hull.fill(150);
    hull.vertex( - 50, 0);
    hull.vertex( - 40, 25);
    hull.vertex(0, 25);
    hull.vertex(0, -7);
    hull.endShape();
    boat.addChild(hull);

    PShape hullHighlight = createShape();
    hullHighlight.beginShape(QUAD);
    hullHighlight.stroke(180);
    hullHighlight.fill(180);
    hullHighlight.vertex(0, -7);
    hullHighlight.vertex(0, 25);
    hullHighlight.vertex(40, 25);
    hullHighlight.vertex(50, 0);
    hullHighlight.endShape();
    boat.addChild(hullHighlight);
     
    boat.translate(x, y);
    boat.rotate(startRotation);
    strokeWeight(strokeWeight); 
  }

  void plot() {
    shape(boat);
  }

  void rotate(float angle) {
    boat.rotate(angle);
  }

}
