// PRNG courtesy of PracRand
function sfc32(a, b, c, d) {
    return function () {
        a >>>= 0; b >>>= 0; c >>>= 0; d >>>= 0;
        var t = (a + b) | 0;
        a = b ^ b >>> 9;
        b = c + (c << 3) | 0;
        c = (c << 21 | c >>> 11);
        d = d + 1 | 0;
        t = t + d | 0;
        c = c + t | 0;
        return (t >>> 0);
    }
}

var rand = undefined;
var rawHash = undefined;
var hash = undefined;
var hashWidth;
var hashHeight;

function randint(from, to) {
    return rand() % (to - from) + from;
}

function rrand() {

    return rand() / 4294967296;
}

function createHashCanvas(width = null, height = null, renderer = P2D)
{
    var params = new URLSearchParams(window.location.search);
    if (!params.has("hash")) {
        console.warn("The hash param is missing.");
        rawHash = "0";
        hash = 0;
    } else {
        rawHash = params.get("hash");
        hash = parseInt(rawHash.substr(0, 8), 16);
    }
    noiseSeed(hash);
    randomSeed(hash);
    rand = sfc32(0, 4, hash, 1);
    for (let i = 0; i < 10; ++i) {
        rand();
    }

    hashWidth = parseInt(params.get("width") ?? "1024");
    hashHeight = parseInt(params.get("height") ?? "1024");
    width = width ?? hashWidth;
    height = height ?? hashHeight;

    let canvas = createCanvas(width, height, renderer);
    pixelDensity(1);

    day = () => randint(0, 31) + 1;
    hour = () => randint(0, 24);
    minute = () => randint(0, 60);
    millis = () => randint(0, 1000);
    month = () => randint(0, 12) + 1;
    second = () => randint(0, 60);
    year = () => randint(0, 8192);
    return canvas;
}

function saveHashCanvas(name)
{
    saveCanvas(`${name}_${rawHash}`, "png");
}

function saveHashGraphics(name, graphics)
{
    save(graphics, `${name}_${rawHash}.png`);
}

function hx() { return randint(0, width); }
function hy() { return randint(0, height); }
