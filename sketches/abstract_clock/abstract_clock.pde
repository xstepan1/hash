/**
 * @name Abstract Clock
 * @author Jano Popeláš <janpopelas at mail.muni.cz>
 **/

import java.lang.Math; 
import java.util.ArrayList;
import processing.svg.*;
import processing.pdf.*;

/* Creative Constraint chosen:
 *     Primary:   Osciluj
 *     Secondary: Využij goniometrické funkce pro pohyb.
 */

/*
 * @author: Jan Popelas
 *
 * This is a main file of abstract clock project.
 *
 * Controls:
 *     A: saves current clock frame as png file
 *     S: saves current clock frame as svg file
 *     D: saves current clock frame as pdf file
 *     C: changes display of seconds from smooth to clicky (and vice versa)
 */

// Pallette
color watchFaceBGColor = #000000;
color watchFaceFGColor = #4361EE;
color armSecondsColor = #4CC9F0;
color armMinutesColor = #F72585;
color armHoursColor   = #A329F5; 

// Global variables for watch arms
OscillationArrow hoursArm;
OscillationArrow minutesArm;
OscillationArrow secondsArm;

// Other global variables
int initialSecond;
boolean saveSVG = false;
boolean savePDF = false;
boolean drawMillis = true;
String imageFilePrefix = "Jano_Popelas";

/**
 * Function draws the background of the watchface
 */
void drawWatchfacePrologue() {
    background(0);
    noStroke();
    
    // Draw gradient background
    int steps = 50;
    float t = 1;
    float maxSize = max(width, height) * sqrt(2);
    float minSize = min(width, height);
    for (int i = 0; i <= steps; i++) {
        float size = map(t, 0, 1, minSize, maxSize);
        color c = lerpColor(watchFaceFGColor, watchFaceBGColor, t);
        fill(c);
        ellipse(width / 2, height / 2, size, size);
        t -= 1f / steps;
    }
    
    // Draw smaller black ring
    fill(0);
    ellipse(width / 2, height / 2, minSize - 3, minSize - 3);

    // Draw dot indicators for minutes and hours
    float theta = 0;
    float pointRad = minSize / 2 - 20;
    fill(watchFaceFGColor);
    for (int i = 0; i < 60; i++) {
        float circleRad;
        if (i % 5 == 0) circleRad = 10; // Hour indicator
        else circleRad = 6; // Minute indicator

        float x = pointRad * cos(theta) + width / 2;
        float y = pointRad * sin(theta) + height / 2;

        ellipse(x, y, circleRad, circleRad);
        theta += radians(6); 
    }
}

void setupHash() {
    // size(1000, 1000);
    background(33);
    initialSecond = hsecond();

    int arrowCount = 5;

    // Arm setup
    hoursArm = new OscillationArrow(arrowCount, -10, 2);
    hoursArm.setColor(armHoursColor);
    hoursArm.setArmLength(int(min(width, height) / 4.2));

    minutesArm = new OscillationArrow(arrowCount, -8, 5);
    minutesArm.setColor(armMinutesColor);
    minutesArm.setArmLength(int(min(width, height) / 3));

    secondsArm = new OscillationArrow(arrowCount, -6, 3);
    secondsArm.setColor(armSecondsColor);
    secondsArm.setArmLength(int(min(width, height) / 2.2));

    drawWatchfacePrologue();
}

/**
 * Function erases watch arms
 */
void eraseWatchArms() {
    fill(watchFaceBGColor);
    ellipseMode(CENTER);
    ellipse(width / 2, height / 2, min(width, height) - 50, min(width, height) - 50);
}

/**
 * Function draws each of the arms, either in clicky or in smooth mode
 */
void drawWatchArms() {
    if (drawMillis)
        secondsArm.drawWatchArm(initialSecond * radians(6) + float(hmillis()) / 1000 * radians(6));
    else
        secondsArm.drawWatchArm(hsecond() * radians(6));
    minutesArm.drawWatchArm(hminute() * radians(6) + hsecond() * radians(6) / 60);
    hoursArm.drawWatchArm((hhour() % 12) * radians(30) + hminute() * radians(30) / 60 + hsecond() * radians(30) / 3600);
}

/**
 * Function draws center dot of the watchface
 */
void drawWatchfaceEpilogue() {
    fill(watchFaceBGColor);
    ellipseMode(CENTER);
    ellipse(width / 2, height / 2, 20, 20);
}

void drawHash() {
    if (saveSVG) {
        beginRecord(SVG, imageFilePrefix + "-" + str(hour()) + "-" + str(minute()) + "-" + str(second()) + "-svg-####.svg");
        setup();
    }
    if (savePDF) {
        beginRecord(PDF, imageFilePrefix + "-" + str(hour()) + "-" + str(minute()) + "-" + str(second()) + "-pdf-####.pdf");
        setup();
    }

    eraseWatchArms();
    drawWatchArms();
    drawWatchfaceEpilogue();

    if (saveSVG || savePDF) {
        endRecord();
        saveSVG = false;
        savePDF = false;
    }
}

void keyPressed() {
    if (key == 'a' || key == 'A') saveFrame(imageFilePrefix + "-png-####.png");
    if (key == 's' || key == 'S') saveSVG = true;
    if (key == 'd' || key == 'D') savePDF = true;
    if (key == 'c' || key == 'C') drawMillis = !drawMillis;
}
