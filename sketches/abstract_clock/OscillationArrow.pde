class OscillationArrow {
    // Oscillation constants
    private static final float expFunc = 0.8;   // Exponential function for decrease of width
    private static final float thetaInc = 0.02; // Defines how periodic sinus oscillation should be

    // Noise related constants
    private static final float beginN = 0.01;
    private static final float endN = 1;      
    private static final float beginS = 0.0005;
    private static final float endS = 0.005;

    // Arm size
    private static final float proportion = 1/10f; // Defines mid-points in arm in range (0, 1)
    private static final float maxAmplitude = 70; // Max width of arm

    // Arm shape and curves
    private int numberOfCurves; // Number of oscillating curves
    private float initExp;    // initial value of function expFunc^x
    private float endExp;      // ending value of function expFunc^x
    
    // Oscillation curve related data
    private ArrayList<Float> noiseStart = new ArrayList<Float>();
    private ArrayList<Float> noiseStep = new ArrayList<Float>();
    private ArrayList<Float> startPos = new ArrayList<Float>();

    // Other optional settings
    private color armColor = #FFFFFF;
    private int armLength = width / 2;

    public OscillationArrow(int linesCount, float start, float end) {
        this.numberOfCurves = linesCount;
        this.initExp = start;
        this.endExp = end;
        for (int i = 0; i < linesCount; i++) {
            this.noiseStart.add(random(beginN, endN));
            this.noiseStep.add(random(beginS, endS));
            this.startPos.add(random(0, TWO_PI));
        }
    }

    public void setColor(color c) {
        this.armColor = c;
    }

    public void setArmLength(int length) {
        this.armLength = length;
    }

    private void calculateNoise(int value) {
        for (int i = 0; i < this.numberOfCurves; i++) {
            this.noiseStart.set(i, noise(this.noiseStart.get(i) + this.noiseStep.get(i) * value));
        }
    }

    private void drawOscillationArm(float theta) {
        fill(this.armColor);

        float ex = this.endExp;
        float inc = - abs(this.initExp - this.endExp) / (this.armLength * this.proportion);

        for (int x = 0; x < this.armLength * this.proportion; x++) {
            float val = (float) Math.pow(this.expFunc, ex);
            val = map(val, (float) Math.pow(this.expFunc, this.endExp), (float) Math.pow(this.expFunc, this.initExp), 0f, 1f);
            float y = val * maxAmplitude * sin(theta);
            ex += inc;
            theta += this.thetaInc;
            ellipse(x, y, 5, 5);
        }

        ex = this.initExp;
        inc = abs(this.initExp - this.endExp) / (this.armLength * (1f - this.proportion) );

        for (int x = this.armLength / 10; x < this.armLength; x++) {
            float val = (float) Math.pow(this.expFunc, ex);
            val = map(val, (float) Math.pow(this.expFunc, this.endExp), (float) Math.pow(this.expFunc, this.initExp), 0f, 1f);
            float y = val * maxAmplitude * sin(theta);
            ex += inc;
            theta += this.thetaInc;
            ellipse(x, y, 5, 5);
        }
    }

    public void drawWatchArm(float angle) {
        fill(armSecondsColor);
        
        pushMatrix();
        translate(width / 2, height / 2);
        rotate(radians(270) + angle);
        for (int i = 0; i < this.numberOfCurves; i++) {
            this.calculateNoise(frameCount);
            float theta = map(this.noiseStart.get(i), 0, 1, 0, TWO_PI) + this.startPos.get(i);
            this.drawOscillationArm(theta);
        }
        popMatrix();
    }
}