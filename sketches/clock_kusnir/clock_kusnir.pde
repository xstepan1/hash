color yellow = #FFDC00;
color orange = #FF9007;
color orangeLight = #FFC037;
color green = #1FAA50;
color greenLight = #4FDA90;
color grey = #BDBBBA;
color greyLight = #EDEBEA;
color blue = #8FC2DB;
color blueDark = #3F728B;

PImage img;

void setupHash() {
  // size(1000, 1000);
  background(0);
  rectMode(CENTER);
  noStroke();
  //for creating and storing the background
  stars();
  saveFrame("data/stars.png");
  
  img = loadImage("stars.png");
  
  strokeCap(SQUARE);
}

void drawHash() {
  image(img, 0, 0);
  
  fill(yellow);
  translate(width/2, height/2);
  scale(min(width / 1000, height / 1000));
  circle(0, 0, 100);
  
  pushMatrix();
  rotate((( (hsecond() + secFracs()) * TWO_PI) / 60));
  translate(0, -100);
  fill(grey);
  circle(0, 0, 25);
  fill(greyLight);
  arc(0, 0, 25, 25, 0, PI);
  popMatrix();
  
  
  pushMatrix();

  rotate(((hminute() + ((float)hsecond() / 60) ) * TWO_PI ) / 60);
  translate(0, -250);
  fill(green);
  circle(0, 0, 50);
  fill(greenLight);
  arc(0, 0, 50, 50, 0, PI);
  noFill();

  strokeWeight(2);
  for(int i = 0; i < 5; i++){
    float radius = 75 + i * 8;
    stroke(blue);
    circle(0, 0, radius);
    stroke(blueDark);
    arc(0, 0, radius, radius, PI + QUARTER_PI +0.1 * i, TWO_PI - QUARTER_PI - 0.1 * i); 
  }
  popMatrix();
  noStroke();

  rotate(((hhour() % 12 + ((float)hminute()/ 60)) * TWO_PI) / 12 );
  translate(0, -400);
  fill(orange);
  circle(0, 0, 40);
  fill(orangeLight);
  arc(0, 0, 40, 40, 0, PI);
  rotate( - frameCount * 0.001);
  translate(40, 40);
  fill(greyLight);
  circle(0, 0, 20);
  
  
}

float secFracs(){
  return ((float) (hmillis() % 1000) / 1000 );
  
}


void stars(){
  colorMode(HSB, 360, 100, 100);
  for (int i = 0; i < 500; i++){
    fill(random(0, 65), random(60), random(60, 100));
    noStroke();
    circle(random(width), random(height), random(4));
  }
  colorMode(RGB);
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("jan_kusnir_Clock_"+
  second() + "sec" + minute() + "min" + hour() + "hr" + ".png");
  if (key == 'l') loop();
  if (key == 'n') noLoop();
}
