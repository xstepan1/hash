int hash = 0;
String outDir = "";
Boolean hasManyFrames = false;
int maxFrames = 1024;
int minFrames = 128;

void settings() {
    if (args != null) {
        int seed = Integer.parseInt(args[0]);
        int w = Integer.parseInt(args[1]);
        int h = Integer.parseInt(args[2]);
        outDir = args[3];
        size(w, h);
        randomSeed(seed);
        noiseSeed(seed);
        hash = seed;
    } else {
        size(1024, 1024);
        randomSeed(0);
        noiseSeed(0);
    }
}

void setup() {
    setupHash();
    if (hasManyFrames) {
        drawMany();
    } else {
        drawHash();
    }
    String name = new File(sketchPath("")).getName();
    save(outDir + "/" + name + "_" + hash + ".png");
    exit();
}

void drawMany() {
    int iterations = (int)random(minFrames, maxFrames);
    for (int i = 0; i < iterations; ++i) {
      drawHash();
    }
}

int randint(int from, int to) { return ((int)random(0, 2147483647) % (to - from)) + from; }
int hyear() { return randint(0, 8192); }
int hmonth() { return randint(0, 12) + 1; }
int hday() { return randint(0, 31) + 1; }
int hhour() { return randint(0, 24); }
int hminute() { return randint(0, 60); }
int hsecond() { return randint(0, 60); }
int hmillis() { return randint(0, 1000); }
int hx() { return randint(0, width); }
int hy() { return randint(0, height); }
