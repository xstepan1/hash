/**
 * @name Hashdecks
 * @author Adam Štěpánek <469242 at mail.muni.cz>
 **/

// @ts-check

class DeckOptions {
    funColor = null;
    width = 2048;
    height = 2048;
    margin = 64;
    step = 16;
    spacing = 32;
    cardSize = 64;
    cardCount = 8;
}

var funSinhModule = -1;

var funColors = [
    (i, j) => (i * j) % 256,
    (i, j) => [Math.clz32(i) * 8 - Math.clz32(j) * 4, Math.clz32(j) * 8 - Math.clz32(i) * 4, Math.clz32(i % j) * 8],
    (i, j) => [Math.sin(i) * 256, 0, Math.cos(j) * 256],
    (i, j) => [(i * i + j * j * j) % 256, (i * i * i + j * j * j) % 256, 0],
    (i, j) => [(i + j) % 256, (i * j) % 256, Math.pow(i, j) % 256],
    (i, j) => [~(i & j) % 256, ~(i | j) % 256, (i ^ j) % 256],
    (i, j) => {
        if (funSinhModule == -1) {
            funSinhModule = random(7, 700);
        }
        return [Math.sinh((i * j) % funSinhModule) % 256]
    }
];

/**
 * Draws a grid of square gray card decks.
 * @param {DeckOptions} options Parameters of the generator.
 */
function decks(options) {
    push();
    const {funColor, width, height, margin, step, spacing, cardSize, cardCount} = options;
    // resizeCanvas(width, height);
    background(256);

    noStroke();

    var effWidth = floor(width - 2 * margin);
    var effHeight = floor(height - 2 * margin);

    var deckSize = Math.max(-cardSize + 1, cardSize + (cardCount - 1) * step);
    var horDeckCount = Math.max(2, floor(effWidth - 2 * margin) / deckSize);
    var verDeckCount = Math.max(2, floor(effHeight - 2 * margin) / deckSize);
    
    var spacedDeckWidth = deckSize + ((horDeckCount - 1) * spacing) / horDeckCount;
    var spacedDeckHeight = deckSize + ((verDeckCount - 1) * spacing) / verDeckCount;

    horDeckCount = Math.max(2, floor(effWidth / spacedDeckWidth));
    verDeckCount = Math.max(2, floor(effHeight / spacedDeckHeight));

    // var realSpacing = Math.min(spacedDeckSize, (relevantSize - deckCount * deckSize) / (deckCount - 1));
    var horSpacing = Math.min(spacedDeckWidth, (effWidth - horDeckCount * deckSize) / (horDeckCount - 1));
    var verSpacing = Math.min(spacedDeckHeight, (effHeight - verDeckCount * deckSize) / (verDeckCount - 1));
    spacedDeckWidth = deckSize + ((horDeckCount - 1) * horSpacing) / horDeckCount;
    spacedDeckHeight = deckSize + ((verDeckCount - 1) * verSpacing) / verDeckCount;
    console.log(`deckCount: ${horDeckCount} x ${verDeckCount}, realSpacing: ${horSpacing} x ${verSpacing}`);

    var startX = width / 2 - (spacedDeckWidth * horDeckCount + 2 * margin) / 2;
    var startY = height / 2 - (spacedDeckHeight * verDeckCount + 2 * margin) / 2;
    translate(startX, startY);

    for (var deckY = 0; deckY < verDeckCount; ++deckY) {
        for (var deckX = 0; deckX < horDeckCount; ++deckX) {
            for (var deckZ = 0; deckZ < cardCount; ++deckZ) {
                var i = deckX * horDeckCount + deckZ;
                var j = deckY * verDeckCount + deckZ;
                fill(funColor(i, j));
                square(
                    margin + deckX * deckSize + deckX * horSpacing + deckZ * step,
                    margin + deckY * deckSize + deckY * verSpacing + deckZ * step,
                    cardSize);
            }
        }
    }
    pop();
}

function setup() {
    createHashCanvas();

    var options = new DeckOptions();
    options.cardSize = pow(2, randint(4, 9));
    options.cardCount = pow(2, randint(1, 6));
    options.margin = randint(0, pow(2, randint(0, 6)));
    options.width = width;
    options.height = height;
    options.step = randint(-options.cardSize + 1, pow(2, randint(0, 6)));
    options.spacing = randint(-options.cardSize - options.step + 1, pow(2, randint(0, 6)));
    options.funColor = funColors[randint(0, funColors.length)];
    console.log(options);
    decks(options);

    saveHashCanvas("hashdecks");
}
