/**
 * @name Squares
 * @author Tamara Babálová <485550 at mail.muni.cz>
 **/

int size = 9;
int offset = 50;
float w, h;

void setupHash() {
  // size(800, 800);
  colorMode(RGB, 255);
  noFill();    
  w = (width - 40 * 2) / 8;
  h = (height - 40 * 2) / 8;
}

void drawHash() {
  strokeWeight(2);
  int v1 = int(random(100,255));
  int v2 = int(random(100,255));
  int v3 = int(random(100,255));
  stroke(v1,v2,v3);
  int roff = int(random(2,20));
  int goff = int(random(2,20));
  int boff = int(random(2,20));
  int degree = int(random(0,180));
  background(#FFFFFF);
  for (int j = 0; j < size; j++) {
    for (int i = 0; i < size; i++) {
      if(j == i){
        v1-=roff; 
        v2-=goff; 
        v3-=boff;
        stroke(v1,v2,v3);
      }
      float x = map(i, 0, size, offset, width - offset);
      float y = map(j, 0, size, offset, height - offset);
      push();
      translate(x + w / 2, y + h / 2);
      rotate(floor(degree) * 360);
      rectMode(CENTER);
      rect(0,0,w,h);
      line(-w / 2, h / 2, w / 2, -h / 2);
      pop();
    }
  }
  noLoop();
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("tamara_babalova_squares_" + year() + day() + hour() + minute() + second() + millis());
}
