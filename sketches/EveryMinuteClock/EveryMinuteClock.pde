/**
 * @name EveryMinuteClock
 * @author Juraj Ulbrich <445367 at mail.muni.cz>
 **/

String timestamp;

int hour;
int minute;
int h1, m1;

int workStart = 9;
int workEnd = 16;

int sleepStart = 22;
int sleepEnd = 7;

boolean wantLines, wantDeviders = false;


void setupHash() {
    
  timestamp = hyear() + nf(hmonth(),2) + nf(hday(),2) + '-' + nf(hhour(),2) + nf(hminute(),2) + nf(hsecond(),2);
  println(timestamp);
  
  // size(312, 390);      // apple watch size
  
  background(33);
  colorMode(HSB, 360, 100, 100, 100);
  frameRate(1);
  
}

void drawHash() {
  background(33);
  hour = hhour();
  minute = hminute();
  
  if(wantDeviders) {
    noStroke();
  } else {
    strokeWeight(0.5);
    stroke(0, 0, 10);
  }


  h1 = 0;
  for (float h = 0; h <= 24; h++) {
    m1 = 1;
    for (float m = 0; m <= 60; m++) {
      
      // coloring
      if (h1 >= workStart && h1 <= workEnd) {       // work
        if (h1 < hour) {
          fill(200, 50, 50);
        } else {
          if (h1 == hour && m1 < minute) {
            fill(200, 50, 50);
          } else {
            fill(200, 50, 80);
          } 
        }
      } else if(h1 >= sleepStart || h1 <= sleepEnd){      // sleep
          if (h1 < hour) {
          fill(0, 0, 20);
        } else {
          if (h1 == hour && m1 < minute) {
            fill(0, 0, 20);
          } else {
            fill(0, 0, 40);
          } 
        }
      } else {        // free time
          if (h1 < hour) {
          fill(140, 50, 50);
        } else {
          if (h1 == hour && m1 < minute) {
            fill(140, 50, 50);
          } else {
            fill(140, 50, 80);
          } 
        }
      }
      
      
      // actual time
      if (h1 == hour && m1 == minute) {
        fill(0, 100, 100);
      }
      
      rect(h * width/24, m * height/60, (h * width/24) + width/24, (m * height/60) + height/60);
      m1++;
    }
    h1++;
  }
  
  // lines for easier orientation
  if(wantLines) {
    stroke(0,0,100);
    strokeWeight(1);
    
    int lines = 6; //<>//
    float line1 = 0;
    for (int i = 1; i < lines; i++) {
      line1 += height / lines;
      line(0, line1, width, line1);
    }
    line(width/2, 0, width/2, height);
  }
}

void keyPressed() {
  if (key == 's' || key == 'S') {saveFrame("juraj-ulbrich-labyrinth-" + timestamp + "-####.png");  println("Saved #####");}
  if (key == '1') wantLines = !wantLines;
  if (key == '2') wantDeviders = !wantDeviders;

}
