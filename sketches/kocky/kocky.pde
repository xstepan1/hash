/**
 * @name Kocky
 * @author Jakub Kolárovský <485605 at mail.muni.cz>
 **/

String timestamp;

float brightnessSaturationDiviation = 50;
float hueDiviation = 30;
float baseHue = 0;
int fps = 2;
float[] angles = new float[16*100];

int rows;
int cols;
boolean[] skipCubes;

boolean recording = false;
boolean doubleRandom = false;
boolean aroundMouse = false;
boolean inverseMouse = false;
int mouseRadius = 100;

void setupHash() {
  cols = width / 50;
  rows = height / 50;
  skipCubes = new boolean[rows * cols];

  // size(720, 720);
  frameRate(15);
  
  colorMode(HSB, 360, 100, 100, 100); 
  background(0, 20, 0);
  timestamp = hyear() + nf(hmonth(), 2) + nf(hday(), 2) + "-"  + nf(hhour(), 2) + nf(hminute(), 2) + nf(hsecond(), 2);
  
  fill(0, 0, 0, 50);
  noStroke();
  
  for (int i = 0; i < angles.length; i++) {
    angles[i] = random(360);
  }
  
  skipRandomCubes();
}

void skipRandomCubes() {
  for (int i = 0; i < skipCubes.length; i++) {
    if (random(1) < 0.5)
      skipCubes[i] = true;
     else
      skipCubes[i] = false;
  }
}

void drawCube(float x, float y, float size, color col, float angle) {
  float halfSize = size / 2;
  
              //a             b             c  d         e             f         g
  float[] xs = {x - halfSize, x + halfSize, x, x + size, x - halfSize, x       , x + size};
  float[] ys = {y - halfSize, y - halfSize, y, y       , y + halfSize, y + size, y + size};
  
  float offset = random(-10, 10);
  float cosinus = cos(radians(angle));
  float sinus = sin(radians(angle));
  for (int i = 0; i < xs.length; i++) {
    xs[i] -= x + offset;
    ys[i] -= y + offset;
    float newXs = xs[i] * cosinus - ys[i] * sinus;
    float newYs = xs[i] * sinus + ys[i] * cosinus;
    xs[i] = newXs + x + offset;
    ys[i] = newYs + y + offset;
  }
  
  fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[1], ys[1], xs[3], ys[3]);
  if (doubleRandom)
    fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[2], ys[2], xs[3], ys[3]);
  
  fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[2], ys[2], xs[5], ys[5]);
  if (doubleRandom)
    fill(randomizeColor(col, 15, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[4], ys[4], xs[5], ys[5]);
  
  fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[2], ys[2], xs[3], ys[3], xs[5], ys[5]);
  if (doubleRandom)
    fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[3], ys[3], xs[5], ys[5], xs[6], ys[6]);
}

void drawFront(float x, float y, float size, color col) {
  float halfSize = size / 2;
  
              //a             b             c  d         e             f         g
  float[] xs = {x - halfSize, x + halfSize, x, x + size, x - halfSize, x       , x + size};
  float[] ys = {y - halfSize, y - halfSize, y, y       , y + halfSize, y + size, y + size};
  
  fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[2], ys[2], xs[3], ys[3], xs[5], ys[5]);
  if (doubleRandom) fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[3], ys[3], xs[5], ys[5], xs[6], ys[6]);
}

void drawSide(float x, float y, float size, color col) {
  float halfSize = size / 2;
  
              //a             b             c  d         e             f         g
  float[] xs = {x - halfSize, x + halfSize, x, x + size, x - halfSize, x       , x + size};
  float[] ys = {y - halfSize, y - halfSize, y, y       , y + halfSize, y + size, y + size};
  
  fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[2], ys[2], xs[5], ys[5]);
  if (doubleRandom) fill(randomizeColor(col, 15, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[4], ys[4], xs[5], ys[5]);
}

void drawTop(float x, float y, float size, color col) {
  float halfSize = size / 2;
  
              //a             b             c  d         e             f         g
  float[] xs = {x - halfSize, x + halfSize, x, x + size, x - halfSize, x       , x + size};
  float[] ys = {y - halfSize, y - halfSize, y, y       , y + halfSize, y + size, y + size};
  
  fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[1], ys[1], xs[3], ys[3]);
  if (doubleRandom) fill(randomizeColor(col, hueDiviation, brightnessSaturationDiviation));
  triangle(xs[0], ys[0], xs[2], ys[2], xs[3], ys[3]);
}

color randomizeColor(color base, float diviationH, float diviation) {  
  float newHue = hue(base) + random(-diviationH, diviationH);
  float newSaturation = saturation(base) + random(-diviation, diviation);
  float newBrightness = brightness(base) + random(-diviation, diviation);
  return color(newHue, newSaturation, newBrightness);
}

void randomStationaryCubes() {
  background(0, 20, 0);

  for (int j = 0; j < rows; j++) {
    for (int i = 0; i < cols; i++) {
      if (aroundMouse) {
        if ((mouseY - mouseRadius >= j * 50 || j * 50 >= mouseY + mouseRadius)
        || (mouseX - mouseRadius >= i * 50 || i * 50 >= mouseX + mouseRadius))
        {
          continue;
        }
      }
      
      if (skipCubes[j*cols + i])
        drawTop(i * 50, j * 50, 50, color(baseHue % 360, 100, 100));
    }
  }
  
  for (int j = 0; j < rows; j++) {
    for (int i = 1; i < cols; i++) {
      if (aroundMouse) {
        if ((mouseY - mouseRadius >= j * 50 || j * 50 >= mouseY + mouseRadius)
        || (mouseX - mouseRadius >= i * 50 || i * 50 >= mouseX + mouseRadius))
        {
          continue;
        }
      }
      
      if (!skipCubes[j*cols + i - 1] && skipCubes[j*cols + i])
        drawSide(i * 50, j * 50, 50, color(baseHue % 360, 100, 100));
    }
  }
  
  for (int j = 0; j < rows; j++) {
    for (int i = 0; i < cols; i++) {
      if (aroundMouse) {
        if ((mouseY - mouseRadius >= j * 50 || j * 50 >= mouseY + mouseRadius)
        || (mouseX - mouseRadius >= i * 50 || i * 50 >= mouseX + mouseRadius))
        {
          continue;
        }
      }
      
      if (skipCubes[j*cols + i])
        drawFront(i * 50, j * 50, 50, color(baseHue % 360, 100, 100));
    }
  }
  
  baseHue += 10;
}

void randomDancingCubes() {
  background(0, 20, 0);
  for (int j = -8; j < 8; j++) {
    for (int i = 0; i < 100; i++) {
      int angleId = (j+8) * 100 + i;
      drawCube(i * 50 + j * 100, i * 50, 50, color(baseHue % 360, 100, 100), angles[angleId]);
      angles[angleId] += 30;
    }
  }
  
  baseHue += 10;
}

void drawHash() {
  randomStationaryCubes();
  //randomDancingCubes();
  if (recording && frameCount % fps == 0) saveFrame(timestamp + "/Jakub_Kolarovsky_kocky" + "_####.png");
}

// function called when any key is pressed by user
void keyPressed() {
  // if 's' key is pressed, save the current frame 
  if (key == 's' || key == 'S') saveFrame(timestamp + "/Jakub_Kolarovsky_kocky" + "_####.png");
  if (key == 'r' || key == 'R') recording = !recording;
  if (key == 'q' || key == 'Q') skipRandomCubes();
  if (key == 'w' || key == 'W') doubleRandom = !doubleRandom;
  if (key == 'm') aroundMouse = !aroundMouse;
  if (key == 'M') inverseMouse = !inverseMouse;
}
