/**
 * @name Hashclock
 * @author Adam Štěpánek <469242 at mail.muni.cz>
 **/

function setup() {
    createHashCanvas();
    state = new State();
    state.setTime(new Date(
        randint(0, 2021),
        randint(0, 12),
        randint(1, 32),
        randint(0, 24),
        randint(0, 60),
        randint(0, 60),
        randint(0, 1000)));
    noStroke();
    translate(width / 2, height / 2);
    scale(min(width, height) / (128 + 8));
    background(32);

    state.setTime(new Date(randint(0, 86400000)));
    state.drawMaze();
    state.drawClock();
    saveHashCanvas("hashclock");
}
