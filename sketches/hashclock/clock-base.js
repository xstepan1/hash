/**
 * @name Hashclock (base)
 * @author Adam Štěpánek <469242 at mail.muni.cz>
 **/

// @ts-check

let addendumFactor = 1;
let dedendumFactor = 1.25;
let outsideGearCount = 5

class Vec {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }

    add(other) {
        return new Vec(this.x + other.x, this.y + other.y);
    }

    sub(other) {
        return new Vec(this.x - other.x, this.y - other.y);
    }

    mul(other) {
        return new Vec(this.x * other.x, this.y * other.y);
    }

    mulc(value) {
        return new Vec(this.x * value, this.y * value);
    }

    div(other) {
        return new Vec(this.x / other.x, this.y / other.y);
    }

    divc(value) {
        return new Vec(this.x / value, this.y / value);
    }

    mag() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    norm() {
        let m = this.mag();
        return new Vec(this.x / m, this.y / m);
    }

    atan2() {
        return Math.atan2(this.y, this.x);
    }

    rotate(angle) {
        return new Vec(this.x * cos(angle) - this.y * sin(angle),
            this.x * sin(angle) + this.y * cos(angle));
    }
};

/**
 * @param {Vec} point 
 * @param {*} c 
 */
function addNoise(point, c) {
    let n = noise(point.x / 2, point.y / 2) * 2 - 1;
    n = floor(n * 20);
    return color(red(c) + n, green(c) + n, blue(c) + n);
}

class Gear {
    constructor(module, pressureAngle, teethCount, teethScale = 1) {
        this.module = module;
        this.pressureAngle = pressureAngle;
        this.teethCount = teethCount;
        this.teethScale = teethScale;
        this.diameter = module * teethCount;
        this.addendum = addendumFactor * module * teethScale;
        this.dedendum = dedendumFactor * module * teethScale;
        this.angle = 0;
        this.phase = 0;
        this.internal = null;
        this.isInternal = false;
    }

    neighbor(teethCount) {
        return new Gear(this.module, this.pressureAngle, teethCount, this.teethScale);
    }

    makeInternal(module, teethScale = this.teethScale) {
        if (this.internal || this.isInternal) {
            throw new Error("This gear already has an internal gear or is an internal gear!");
        }
        this.internal = new Gear(module, this.pressureAngle, this.teethCount, teethScale);
        this.internal.isInternal = true;
        this.internal.parent = this;
    }

    meshWith(parent) {
        this.parent = parent;
    }

    meshInside(parent) {
        if (parent.internal) {
            parent = parent.internal;
        }
        if (!parent.isInternal) {
            throw new Error("You can only mesh inside an internal gear!");
        }
        this.parent = parent;
    }

    static pertex(radius, phi) {
        // it's a Polar Vertex, get it?
        vertex(radius * cos(phi), radius * sin(phi));
    }

    static bezierPertex(r1, phi1, r2, phi2, r3, phi3) {
        bezierVertex(r1 * cos(phi1), r1 * sin(phi1),
            r2 * cos(phi2), r2 * sin(phi2),
            r3 * cos(phi3), r3 * sin(phi3));
    }

    static teeth(module, pressureAngle, teethCount, addendum, dedendum) {
        let pitch = module * PI;
        let radius = abs(module * teethCount / 2);
        let pitchAngle = pitch / radius;
        let bitAngle = addendum / tan(PI / 2 - pressureAngle) / radius;

        let phi = 0;
        for (var i = 0; i < teethCount; ++i) {
            //   _____
            //  /     \        
            // /       \      
            //          |     |
            //          \_____/
            Gear.pertex(radius, phi);
            Gear.pertex(radius + addendum, phi + bitAngle);
            Gear.pertex(radius + addendum, phi + pitchAngle / 2 - bitAngle);
            Gear.pertex(radius, phi + pitchAngle / 2);
            Gear.bezierPertex(radius - dedendum, phi + pitchAngle / 2 + bitAngle,
                radius - dedendum, phi + pitchAngle / 2 + bitAngle,
                radius - dedendum, phi + 3 / 4 * pitchAngle);
            Gear.bezierPertex(radius - dedendum, phi + pitchAngle - bitAngle,
                radius - dedendum, phi + pitchAngle - bitAngle,
                radius, phi + pitchAngle);
            phi += pitchAngle;
        }
    }

    draw() {
        if (!this.isInternal && this.parent) {
            rotate(-this.parent.phase - this.parent.angle);
            rotate(this.angle);

            let gearRatio = this.parent.diameter / this.diameter;
            if (this.parent.isInternal) {
                this.phase = (this.parent.phase + this.parent.angle - this.angle) * gearRatio;
                translate((this.parent.diameter - this.diameter) / 2, 0);
            } else {
                this.phase = PI + (this.angle - this.parent.phase - this.parent.angle) * gearRatio;
                translate((this.parent.diameter + this.diameter) / 2, 0);
            }
        }

        if (this.internal) {
            // couple the internal gear with the outside
            this.internal.angle = this.angle;
            this.internal.phase = this.phase;
        }

        rotate(this.phase);
        if (this.internal) {
            beginShape();
            Gear.teeth(this.module,
                this.pressureAngle,
                this.teethCount,
                this.addendum,
                this.dedendum);
            beginContour();
            Gear.teeth(-this.internal.module,
                this.internal.pressureAngle,
                this.internal.teethCount,
                -this.internal.addendum,
                -this.internal.dedendum);
            endContour();
            endShape(CLOSE);
        } else {
            beginShape();
            Gear.teeth(this.module,
                this.pressureAngle,
                this.teethCount,
                this.addendum,
                this.dedendum);
            endShape(CLOSE);
        }
    }

    getCenter() {
        if (this.isInternal) {
            return this.parent.getCenter();
        }

        if (!this.parent) {
            // assumes that the outer most parent is at the center
            return new Vec(0, 0);
        }

        let center = new Vec(this.parent.diameter / 2, 0);
        center.x += this.parent.isInternal
            ? -this.diameter / 2
            : this.diameter / 2;
        center = center.rotate(this.angle);
        return this.parent.getCenter().add(center);
    }

    /**
     * @param {Vec} point
     */
    contains(point) {
        let center = this.getCenter();
        let d = point.sub(center).mag();

        if (d < this.diameter / 2) {
            return 1;
        }

        if (this.isInternal) {
            let inner = this.diameter / 2 + this.addendum;
            let outer = this.parent.diameter / 2 - this.parent.dedendum;
            let width = outer - inner;
            return Math.max(0, Math.min(1, 1 - (d - inner) / width));
        } else {
            return Math.max(0, Math.min(1, this.diameter / 2 - d));
        }
    }
}

class Graph {

    constructor(layerCount, layerWidth, baseCellCount = 6) {
        this.layerCount = layerCount;
        this.layerWidth = layerWidth;
        this.baseCellCount = baseCellCount;
        this.initialize();
    }

    initialize() {
        let baseCellSize = PI * 2 * this.layerWidth / this.baseCellCount;
        this.cellCounts = [1, this.baseCellCount];
        this.edges = [[]];

        // the first layer is special
        for (let i = 0; i < this.baseCellCount; ++i) {
            this.edges[0].push(i + 1);
            this.edges.push([
                1 + (i + this.baseCellCount - 1) % this.baseCellCount, // left
                1 + (i + 1) % this.baseCellCount, // right
                0 // root
            ]);
        }

        // the others layers are not
        for (let i = 1; i < this.layerCount; ++i) {
            let cellSize = PI * 2 * this.layerWidth * (i + 1) / this.cellCounts[i];
            let hasGrown = cellSize > baseCellSize;
            let cellCount = hasGrown ? 2 * this.cellCounts[i] : this.cellCounts[i];
            this.cellCounts.push(cellCount);
            let start = this.edges.length;
            for (let j = 0; j < cellCount; ++j) {
                let neighbors = [
                    start + (j + cellCount - 1) % cellCount, // left
                    start + (j + 1) % cellCount]; // right
                if (hasGrown) {
                    if (j % 2 === 0) {
                        let ancestor = start - cellCount / 2 + floor(j / 2);
                        this.edges[ancestor].push(start + j);
                    }
                } else {
                    let ancestor = start - cellCount + j;
                    this.edges[ancestor].push(start + j);
                }
                this.edges.push(neighbors);
            }
        }

        this.coords = [];
        for (let i = 0; i <= this.layerCount; ++i) {
            let r = i * this.layerWidth;
            for (let j = 0; j < this.cellCounts[i]; ++j) {
                let phi = j * TWO_PI / this.cellCounts[i];
                this.coords.push(new Vec(r * cos(phi), r * sin(phi)));
            }
        }
    }

    mazify() {
        let mazeEdges = [];
        for (let i = 0; i < this.edges.length; ++i) {
            mazeEdges.push([]);
        }
        let next = [];
        let isMaze = { 0: true };
        for (let i = 0; i < this.edges[0].length; ++i) {
            next.push({ src: 0, dst: this.edges[0][i] });
        }
        while (next.length != 0) {
            let i = Math.floor(rrand() * next.length);
            let current = next[i];
            next.splice(i, 1);
            if ((isMaze[current.src] === true && isMaze[current.dst] !== true)
                || (isMaze[current.src] !== true && isMaze[current.dst]) === true) {
                let src = isMaze[current.src] === true ? current.src : current.dst;
                let dst = isMaze[current.src] === true ? current.dst : current.src;
                mazeEdges[src].push(dst);
                for (let j = 0; j < this.edges[src].length; ++j) {
                    next.push({ src: src, dst: this.edges[src][j] });
                }
                for (let j = 0; j < this.edges[dst].length; ++j) {
                    next.push({ src: dst, dst: this.edges[dst][j] });
                }
                isMaze[dst] = true;
            }
        }
        this.edges = mazeEdges;
    }

    localIndex(globalIndex) {
        let layer = 0;
        let sum = 0;
        while (sum <= globalIndex) {
            sum += this.cellCounts[layer];
            ++layer;
        }
        --layer;
        sum -= this.cellCounts[layer];
        return [layer, globalIndex - sum];
    }

}

class State {
    constructor() {
        this.createClock();
        this.createMaze();
        this.isDrawing = true;
    }

    /**
     * @param {Date} date 
     */
    setTime(date = null) {
        if (date === null) {
            date = new Date();
        }
        this.date = date;
        // correct for local time of the date
        this.time = date.valueOf() - date.getTimezoneOffset() * 60 * 1000;

        // reset the clock
        let resetAngle = -PI / 2;
        this.dayGear.phase = this.time / 1000 / 60 / 3 % 1 * TWO_PI;

        let hour = this.time / (1000.0 * 60.0 * 60.0 * 12) % 1;
        this.hourGear.angle = resetAngle + hour * TWO_PI;

        let minute = this.time / (1000.0 * 60.0 * 60) % 1;
        this.minuteGear.angle = resetAngle + minute * TWO_PI;

        let second = this.time / (1000 * 60) % 1;
        this.secondGear.angle = resetAngle + second * TWO_PI;

        let millisecond = this.time / 1000 % 1;
        this.millisecondGear.angle = resetAngle + millisecond * TWO_PI;

        this.mazeAngle = this.time / 1000 / 60 / 6 % 1 * TWO_PI;
        this.mazeGears[0].phase = this.time / 1000 / 30 % 1 * TWO_PI;

        for (let i = 0; i < this.maze.edges.length; ++i) {
            this.mazeColors[i] = this.getMazeColor(this.maze.coords[i].rotate(this.mazeAngle));
        }
    }

    createClock() {
        this.dayGear = new Gear(2, PI / 10, 64, 0.5);
        this.dayGear.makeInternal(1.7, 1);

        this.hourGear = this.dayGear.internal.neighbor(48);
        this.hourGear.makeInternal(1.3, 0.6);
        this.hourGear.meshInside(this.dayGear);

        this.minuteGear = this.hourGear.internal.neighbor(32);
        this.minuteGear.makeInternal(0.9, 1);
        this.minuteGear.meshInside(this.hourGear);

        this.secondGear = this.minuteGear.internal.neighbor(24);
        this.secondGear.makeInternal(0.5, 0.7);
        this.secondGear.meshInside(this.minuteGear);

        this.millisecondGear = this.secondGear.internal.neighbor(16);
        this.millisecondGear.meshInside(this.secondGear);
    }

    createMazeBranch(index, parentIndex, parentGear) {
        let vector = this.maze.coords[index].sub(this.maze.coords[parentIndex]);
        let targetDiameter = Math.min(this.maze.layerWidth, (vector.mag() - parentGear.diameter / 2) * 2);
        let teethCount = Math.floor(targetDiameter / parentGear.module);
        let g = parentGear.neighbor(teethCount);
        g.angle = vector.atan2();
        g.meshWith(parentGear);
        this.mazeGears[index] = g;

        // TODO: update the coord so that it is the proper gear center

        for (let i = 0; i < this.maze.edges[index].length; ++i) {
            let next = this.maze.edges[index][i];
            this.createMazeBranch(next, index, g);
        }
    }

    createMaze() {
        this.maze = new Graph(8, 7);
        this.maze.mazify();
        this.mazeGears = new Array(this.maze.edges.length);

        let root = new Gear(0.5, PI / 10, 2 * this.maze.layerWidth, 0.5);
        this.mazeGears[0] = root;
        for (let i = 0; i < this.maze.edges[0].length; ++i) {
            let next = this.maze.edges[0][i];
            this.createMazeBranch(next, 0, root);
        }

        this.mazeColors = new Array(this.maze.edges.length);
    }

    getMazeColor(point) {
        let colorFinal = color("#000000");
    
        let inSecond = this.secondGear.internal.contains(point);
        let inMinute = this.minuteGear.internal.contains(point);
        let colorSecond = color("#B73311");
        let colorMinute = lerpColor(color("#c86f00"), color("#4d0c79"), (sin(this.secondGear.angle) + 1) / 2);
        if (inSecond > 0) {
            colorFinal = lerpColor(colorSecond, colorMinute, 1 - inSecond);
            return addNoise(point, colorFinal);
        }
    
        let inHour = this.hourGear.internal.contains(point);
        let colorHour = lerpColor(color("#B73311"), color("#148538"), (sin(this.minuteGear.angle) + 1) / 2);
        if (inMinute > 0) {
            colorFinal = lerpColor(colorMinute, colorHour, 1 - inMinute);
            return addNoise(point, colorFinal);
        }
    
        let inDay = this.dayGear.contains(point);
        let day = (this.time / 1000 / 60 / 60 / 24 + 0.25) % 1;
        let colorDay = lerpColor(color("#ba9200"), color("#1B2F4B"), (sin(day * TWO_PI) + 1) / 2);
        if (inHour > 0) {
            colorFinal = lerpColor(colorHour, colorDay, 1 - inHour);
            return addNoise(point, colorFinal);
        }
    
        if (inDay > 0) {
            colorFinal = lerpColor(colorDay, colorFinal, 1 - inDay);
            return addNoise(point, colorFinal);
        }
    
        return addNoise(point, colorFinal);
    }

    drawClock() {
        push();
        fill(224, 220, 216);
        this.dayGear.draw();

        push();

        fill(208, 204, 200);
        this.hourGear.draw();

        fill(192, 186, 182);
        this.minuteGear.draw();

        fill(178, 174, 170);
        this.secondGear.draw();

        fill(162, 158, 154)
        this.millisecondGear.draw();

        pop();

        // the three outside gears
        for (let i = 0; i < outsideGearCount; ++i) {
            push();
            fill(64, 60, 56);
            let outsideGear = this.dayGear.neighbor(48);
            outsideGear.angle = TWO_PI / outsideGearCount * i + PI / outsideGearCount;
            outsideGear.meshWith(this.dayGear);
            outsideGear.draw();
            pop()
        }
        pop();
    }

    drawDebug() {
        push();
        noFill();
        stroke(255);
        strokeWeight(0.2);

        let millisecondC = this.millisecondGear.getCenter();
        circle(millisecondC.x, millisecondC.y, this.millisecondGear.diameter);

        let secondC = this.secondGear.internal.getCenter();
        circle(secondC.x, secondC.y, this.secondGear.diameter);
        circle(secondC.x, secondC.y, this.secondGear.internal.diameter);

        let minuteC = this.minuteGear.internal.getCenter();
        circle(minuteC.x, minuteC.y, this.minuteGear.diameter);
        circle(minuteC.x, minuteC.y, this.minuteGear.internal.diameter);

        let hourC = this.hourGear.internal.getCenter();
        circle(hourC.x, hourC.y, this.hourGear.diameter);
        circle(hourC.x, hourC.y, this.hourGear.internal.diameter);

        let dayC = this.dayGear.internal.getCenter();
        circle(dayC.x, dayC.y, this.dayGear.diameter);
        circle(dayC.x, dayC.y, this.dayGear.internal.diameter);
        pop();
    }

    drawMazeBranch(index) {
        push();
        fill(this.mazeColors[index]);
        this.mazeGears[index].draw();
    
        for (let i = 0; i < this.maze.edges[index].length; ++i) {
            let next = this.maze.edges[index][i];
            this.drawMazeBranch(next);
        }
        pop();
    }

    drawMaze() {
        push();
        rotate(this.mazeAngle);
        this.drawMazeBranch(0);
        pop();
    }
}
