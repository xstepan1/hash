/**
 * @name Pattern
 * @author Julka Gonová <456129 at mail.muni.cz>
 **/

 int counter;
 boolean inc = true;
 //int clr = (int) random(360);
 int clr = 0;
 int br = 0;
 int vertices = 6;
 String timestamp;

 int highlightR = 5;
 int highlightC = 5;
 
void setupHash() {
  //size(1500, 1000);
  // fullScreen();
  background(0);
  colorMode(HSB, 360, 100, 100, 100);
}

void drawHash() {
  timestamp = year() + "-" + minute() + "-" + millis();
  frameRate(20);
  fill(0);
  rect(0, 0, width, height);
  background(33);
  
  
  for (int r = 0; r < 11; r++) {
    int init;
    if (r % 2 == 0) {
      init = -1;
    } else {
      init = 0;
    }
    for (int c = init; c < 12; c++) {

      PShape polygon = createShape();
      polygon.beginShape();
      /*
      if (r == highlightR || c == highlightC) {
        polygon.stroke(#FFFFFF);
      } else {
        polygon.stroke(#36F1CD);
      }
      */
      int clr = int(map(hy(), 0, height, 0, 360));
      polygon.stroke(clr, 50, 70);
      polygon.strokeWeight(3);
      polygon.noFill();
      float side = width/10;
      float cutoffFactor = map(hx(), 0, width, -2.5, 2.5);
      cutoffTriangle(polygon, side, cutoffFactor);
      polygon.endShape(CLOSE);
      if (r % 2 == 0) {
        shape(polygon, r*side, c*side - side/2);
      } else {
        shape(polygon, r*side, c*side);
      }     
    }
  }
}

void cutoffTriangle(PShape polygon, float side, float cutoffFactor) {
  float trHeight = (sqrt(3) * side)/2;
  float cutoff = cutoffFactor * side;
  float cutoffHeight = (sqrt(3) * cutoff)/2;
  float finalHeight = trHeight - cutoffHeight;
  //A
  polygon.vertex(-side/2 + cutoff, finalHeight/2);  
  //B
  polygon.vertex(side/2 - cutoff, finalHeight/2);
  //C
  polygon.vertex(side/2 - cutoff/2, finalHeight/2 - cutoffHeight);
  //D
  polygon.vertex(cutoff/2, - finalHeight/2);
  //E
  polygon.vertex(- cutoff/2, - finalHeight/2);  
  //F
  polygon.vertex(- side/2 + cutoff/2, finalHeight/2 - cutoffHeight);
  return;
  }


void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("Julka_Gonova_pattern_" + timestamp + ".png");
  if (keyCode == UP) highlightC--;
  if (keyCode == DOWN) highlightC++;
  if (keyCode == RIGHT) highlightR++;
  if (keyCode == LEFT) highlightR--;
}
