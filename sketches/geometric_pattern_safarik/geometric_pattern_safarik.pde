/**
 * @name Geometric Pattern
 * @author Honza Šafařík <tony.safarik at mail.muni.cz>
 **/

// USAGE
//
// Input:
// press '+': Add more tiles
// press '-': Remove tiles
// press 'c': Randomize colors
// press 'g': Generate new shapes
// press 's': Save screen
//
// press 'r': Show region (DEBUGGING)

TileManager manager;
final int canvasSize = 900;
int tileCount = 4;
boolean showRegion = false;
color clr;

// void settings() {
//   size(canvasSize, canvasSize);
// }

void setupHash() {
  colorMode(HSB, 360, 100, 100, 100);
  clr = color(random(360), random(20, 60), random(40, 70));
  int gridWidth = (int)random(4, 32);
  int gridHeight = (int)(((float)height / width) * (float)gridWidth);
  showRegion = random(0, 100) < 50.0;
  manager = new TileManager(gridWidth, gridHeight, showRegion, clr);
  // reDraw();
}

void drawHash() {
  background((hue(clr)+180)%360, map(saturation(clr), 20, 60, 10, 20), map(brightness(clr), 40, 70, 20, 50));
  manager.draw();
}

// void draw() {
// }

void keyPressed() {
  if (key == 'r') {
    manager.switchRegion();
  }

  // if (key == '+') {
  //   manager.addTiles();
  // }

  // if (key == '-') {
  //   if (tileCount > 1) {
  //     manager.removeTiles();
  //   }
  // }
  
  if (key == 'c') {
    this.clr = color(random(360), random(20, 60), random(40, 70));
    manager.setColor(this.clr);
  }
  
  if (key == 'g') {
    manager.generateTiles();
  }
  
  if (key == 's') {
    StringBuilder builder = new StringBuilder();
    builder.append(str(year()));
    builder.append("-");
    builder.append(str(month()));
    builder.append("-");
    builder.append(str(day()));
    builder.append("-");
    builder.append(str(hour()));
    builder.append("-");
    builder.append(str(minute()));
    builder.append("-");
    builder.append(str(second()));
    builder.append(".png");
    saveFrame(builder.toString());
  }

  // reDraw();
}
