class Tile {

  float x;
  float y;
  int length;
  color clr;

  ArrayList<Integer> patternLength = new ArrayList();
  ArrayList<Integer> patternOffset = new ArrayList();
  
  Boolean draw4 = null;

  public Tile(int x, int y, int length) {
    this.x = x;
    this.y = y;
    this.length = length;
    generateNewPatternValues();
  }

  public void draw(boolean showRegion) {
    if (showRegion) {
      showRegion();
    }
    
    if (this.draw4 == null) {
      this.draw4 = random(int(random(2))) == 0;
    }

    if (draw4) {
      drawMirroredLines8();
    } else {
      drawMirroredLines4();
    }
  }

  void setColor(color clr) {
    this.clr = clr;
  }

  public void generateNewPatternValues() {
    for (int i = 3; i > 0; i--) {
      int scaling = int(length/3) - int(length/5);
      int tmp = int(map(i, 1, 3, 1*scaling, 3*scaling));
      this.patternLength.add(tmp);
      this.patternOffset.add(int(random(tmp)));
    }
  }


  void drawMirroredLines4() {
    noFill();
    stroke(this.clr);
    strokeWeight(5);
    for (int i = 0; i < this.patternLength.size(); i++) {
      int length = this.patternLength.get(i);
      int offset = this.patternOffset.get(i);
      int centerX = int(x + (this.length/2));
      int centerY = int(y + (this.length/2));
      line(centerX - (length/2), centerY - offset, centerX + (length/2), centerY - offset);
      line(centerX - (length/2), centerY + offset, centerX + (length/2), centerY + offset);
      line(centerX - offset, centerY - (length/2), centerX - offset, centerY + (length/2));
      line(centerX + offset, centerY - (length/2), centerX + offset, centerY + (length/2));
    }
  }

  void drawMirroredLines8() {
    noFill();
    stroke(this.clr);
    strokeWeight(5);
    for (int i = 0; i < this.patternLength.size(); i++) {
      int length = this.patternLength.get(i)/2;
      int offset = this.patternOffset.get(i);
      int centerX = int(x + (this.length/2));
      int centerY = int(y + (this.length/2));
      line(centerX - offset, centerY - offset, centerX - offset + length, centerY - offset);
      line(centerX + offset - length, centerY - offset, centerX + offset, centerY - offset);

      line(centerX - offset, centerY + offset, centerX - offset + length, centerY + offset);
      line(centerX + offset - length, centerY + offset, centerX + offset, centerY + offset);

      line(centerX - offset, centerY - offset, centerX - offset, centerY - offset + length);
      line(centerX - offset, centerY + offset - length, centerX - offset, centerY + offset);

      line(centerX + offset, centerY - offset, centerX + offset, centerY - offset + length);
      line(centerX + offset, centerY + offset - length, centerX + offset, centerY + offset);
    }
  }

  public void showRegion() {
    rectMode(CORNER);
    stroke(100, 50, 150);
    strokeWeight(2);
    noFill();
    square(x, y, length);
  }
}
