class TileManager {

  Tile[][] tiles;
  int gridWidth;
  int gridHeight;
  boolean showRegion;
  color baseColor;

  public TileManager(int gridWidth, int gridHeight, boolean showRegion, color clr) {
    this.gridWidth = gridWidth;
    this.gridHeight = gridHeight;
    this.showRegion = showRegion;
    this.baseColor = clr;
    generateTiles();
  }

  void generateTiles() {
    this.tiles = new Tile[this.gridWidth][this.gridHeight];
    for (int i = 0; i < this.gridWidth; i++) {
      for (int j = 0; j < this.gridHeight; j++) {
        int x = int((width/gridWidth)*i);
        int y = int((height/gridHeight)*j);
        int length = (int)min(width / gridWidth, height / gridHeight);
        tiles[i][j] = new Tile(x, y, length);
        this.tiles[i][j].setColor(this.baseColor);
      }
    }
  }

  // public void addTiles() {
  //   this.gridSize++;
  //   generateTiles();
  // }

  // public void removeTiles() {
  //   if (this.gridSize > 1) {
  //     this.gridSize--;
  //     generateTiles();
  //   }
  // }
  
  public void setColor(color clr) {
    this.baseColor = clr;
    updateTilesColor();
  }
  
  public void switchRegion() {
    this.showRegion = !this.showRegion;
    this.draw();
  }

  private void updateTilesColor() {
    for (int i = 0; i < this.gridWidth; i++) {
      for (int j = 0; j < this.gridHeight; j++) {
        this.tiles[i][j].setColor(this.baseColor);
      }
    }
  }

  public void draw() {
    for (int i = 0; i < gridWidth; i++) {
      for (int j = 0; j < gridHeight; j++) {
        tiles[i][j].draw(this.showRegion);
      }
    }
  }
}
