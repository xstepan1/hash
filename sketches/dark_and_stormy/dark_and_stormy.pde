/**
 * @name Dark and Stormy
 * @author Nika Kunzová <nikkun at mail.muni.cz>
 **/

// import processing.pdf.*;

//Dark and stormy
//  Left-click to change position of a spiral
//  Right arrow: gradually change alpha channel of a CIRCLE stroke from 0 to 100 
//  Left arrow: gradually change alpha channel of a LINE stroke from 0 to 100 

//Changeable parameters://

//--------Colours--------//
//Circles//
int defaultHue = 40;      //0 - 360
int alp = 10;             //0 - 100
int colourOffset = 20;
//Circle stroke (Line stroke will be inversed)//
int strokeBri = 0;
//Background (0 - black, 100 - white)
int grey = 0;

//--------Other--------//
int maxIterations = 200;
int iterationsOffset = 50;

float defaultSize = 10;
float defaultR = 0;
float defaultTheta = 0;

//Other changeable parameters are in draw() function on lines 87 - 91
//---------------------//



//Helpers:
float size = defaultSize;
float r = defaultR;
float theta = defaultTheta;

int iterations = 0;
int currentIterations = maxIterations;
color baseColour = color(0);
float colourStep = 0;
int hue = defaultHue;
int bri = 0;

boolean pause;
int xPos = 0;
int yPos = 0;
float lineX = 0;
float lineY = 0;

void setupHash() {
  hasManyFrames = true;
  float s = min(width / 1000, height / 1000);
  maxIterations *= s;
  minFrames = 5 * maxIterations;
  maxFrames = 10 * maxIterations;

  // size(1000, 1000);
  // beginRecord(PDF, "nika_kunzova_dark_and_stormy_006_new.pdf"); 
  
  frameRate(8);
  colorMode(HSB, 360, 100, 100, 100);
  background(0, 0, grey, 100);
  
  updateColour();
  noStroke();
  pause = false;
  
  xPos = width/2;
  yPos = height/2;
  
  lineX = width/2;
  lineY = height/2;
}

void drawHash() {
  
  if(pause) return;
  
  if (iterations >= currentIterations) {
    restart();
    //return;
  }
  
  float x = r * cos(theta);
  float y = r * sin(theta);
  
  updateColour();
  fill(baseColour);
  stroke(0, 0, strokeBri, bri);
  
  //--Change as you like:--//
  theta += 1;
  r += 1;
  size += 1;
  //----------------------//
  
  float newX = x + xPos;
  float newY = y + yPos;
  
  circle(newX, newY, size);
  
  stroke(0, 0, 100 - strokeBri, 100 - bri);
  line(lineX, lineY, newX, newY);
  
  lineX = newX;
  lineY = newY;
  
  iterations++;
}

void restart() {
  r = defaultR;
  theta = defaultTheta;
  size = defaultSize;
  hue = (int)random(defaultHue - colourOffset, defaultHue + colourOffset);
  
  iterations = 0;
  currentIterations = (int)random(maxIterations - iterationsOffset, 
                       maxIterations + iterationsOffset);
  xPos = hx();
  yPos = hy();
}

void updateColour() {
   if (iterations % 25 == 0) {
     hue = (int)random(defaultHue - colourOffset, defaultHue + colourOffset);
   }
   baseColour = color(hue, 100, 100, alp);
}

void keyPressed() {
  if(key == 's' || key == 'S') {
    saveFrame("nika_kunzova_dark_and_stormy_###.jpeg");
    endRecord();
  }
  
  if(key == 'q' || key == 'Q') {
    pause = !pause;
  }
  
  if(keyCode == RIGHT) {
    bri += 10;
    if (bri > 100) bri = 100;
  }
  
  if(keyCode == LEFT) {
    bri -= 10;
    if (bri < 0) bri = 0;
  }
  
}

void mouseClicked() {
  restart();
}
