var __classPrivateFieldSet = (this && this.__classPrivateFieldSet) || function (receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
};
var __classPrivateFieldGet = (this && this.__classPrivateFieldGet) || function (receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
};
/**
 * @name Clockygon
 * @author Peťo Jaško <jasko at mail.muni.cz>
 **/
var _hours, _minutes, _quadrant, _color, _secondary;
/// <reference path="../../lib/types/p5/index.d.ts" />
/// <reference path="../../lib/types/p5/global.d.ts" />
/* creative constraint: Využij goniometrické funkce pro určení barvy. */
// PJ
const mod = (n, m) => ((n % m) + m) % m; // mathematically correct modulo
const cartesian = (theta, r) => [r * sin(theta), r * cos(theta)];
const draw_polygon = (vertices, radius) => {
    beginShape();
    for (let i = 0; i < vertices; ++i)
        vertex(...cartesian(i * TAU / vertices, radius));
    endShape(CLOSE);
};
const translated = (x, y, callback) => {
    push();
    translate(x, y);
    callback();
    pop();
};
const rotated = (angle, callback) => {
    push();
    rotate(angle);
    callback();
    pop();
};
const scaled = (x, y, callback) => {
    push();
    scale(x, y);
    callback();
    pop();
};
class AbstractClock {
    constructor(hours, minutes) {
        _hours.set(this, void 0); // 6-hour clock
        _minutes.set(this, void 0);
        _quadrant.set(this, void 0);
        _color.set(this, void 0);
        _secondary.set(this, void 0);
        __classPrivateFieldSet(this, _quadrant, Math.trunc((hours - 1) / 6) + 1);
        __classPrivateFieldSet(this, _hours, mod(hours - 1, 6) + 1);
        __classPrivateFieldSet(this, _minutes, minutes);
        __classPrivateFieldSet(this, _color, color(map(sin(-HALF_PI + TAU * hours / 24), -1, 1, 190, 430) % 360, 100, 100));
        __classPrivateFieldSet(this, _secondary, color(map(sin(-HALF_PI + TAU * hours / 24), -1, 1, 190, 430) % 360 - 15, 100, 100));
    }
    draw(size = 100) {
        push();
        stroke(__classPrivateFieldGet(this, _color));
        strokeWeight(10).noFill();
        this.draw_hours(size);
        stroke(__classPrivateFieldGet(this, _secondary));
        this.draw_minutes(size);
        noStroke().fill(__classPrivateFieldGet(this, _color));
        this.draw_meridiam(size);
        pop();
    }
    draw_hours(size) {
        if (__classPrivateFieldGet(this, _hours) === 2) {
            [-1, 1].forEach(sign => translated(sign * size / 4, 0, () => draw_polygon(2, size)));
        }
        else {
            draw_polygon(__classPrivateFieldGet(this, _hours) === 1 ? 2 : __classPrivateFieldGet(this, _hours), size);
        }
    }
    draw_minutes(size) {
        const draw_minute_quadrant = () => scaled(1.31, 1, () => rotated(HALF_PI, () => draw_polygon(2, size)));
        const minute_quadrant = Math.floor(__classPrivateFieldGet(this, _minutes) / 15);
        switch (minute_quadrant) {
            case 0:
                draw_minute_quadrant();
                break;
            case 3: // draw an x
                [-1, 1].forEach(sign => rotated(sign * QUARTER_PI, draw_minute_quadrant));
                break;
            default:
                [-1, 1, 0]
                    .slice(0, minute_quadrant + 1)
                    .forEach(sign => translated(0, sign * size * .3, draw_minute_quadrant));
        }
        const CIRCLE_SIZE = 3;
        push();
        fill(__classPrivateFieldGet(this, _color)).noStroke();
        translated(-8 * CIRCLE_SIZE, 1.3 * size, () => {
            let mins = __classPrivateFieldGet(this, _minutes) % 15;
            for (let j = 0; j < 3 && mins; ++j)
                for (let i = 0; i < 5 && mins; ++i, --mins)
                    circle(i * 4 * CIRCLE_SIZE, j * 4 * CIRCLE_SIZE, CIRCLE_SIZE);
        });
        pop();
    }
    draw_meridiam(size) {
        textSize(.13 * size);
        text(__classPrivateFieldGet(this, _quadrant) < 3 ? 'a.m.' : 'p.m.', size, .5 * -size);
    }
}
_hours = new WeakMap(), _minutes = new WeakMap(), _quadrant = new WeakMap(), _color = new WeakMap(), _secondary = new WeakMap();
let hour_slider;
let minute_slider;
function setup() {
    createHashCanvas();
    // hour_slider = createSlider(0, 23, hour(), 1)
    // hour_slider.position(30, 10)
    // minute_slider = createSlider(0, 59, minute(), 1)
    // minute_slider.position(30, 40)
    frameRate(30).colorMode('hsb').fill('white').noStroke().background('#111111');
    drawHash();
    saveHashCanvas("clockygon");
}
function drawHash() {
    blendMode(BLEND);
    background('#111111');
    let h = randint(0, 24);
    let m = randint(0, 60);
    // text(h, 10, 22)
    // text(m, 10, 52)
    blendMode(ADD);
    translate(width / 2, height / 2);
    new AbstractClock(h, m).draw(min(width / 2, height / 2) * 0.67);
}
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
// PRNG courtesy of PracRand
function sfc32(a, b, c, d) {
    return function () {
        a >>>= 0;
        b >>>= 0;
        c >>>= 0;
        d >>>= 0;
        var t = (a + b) | 0;
        a = b ^ b >>> 9;
        b = c + (c << 3) | 0;
        c = (c << 21 | c >>> 11);
        d = d + 1 | 0;
        t = t + d | 0;
        c = c + t | 0;
        return (t >>> 0);
    };
}
var rand = undefined;
var rawHash = undefined;
var hash = undefined;
function randint(from, to) {
    return rand() % (to - from) + from;
}
function rrand() {
    return rand() / 4294967296;
}
function createHashCanvas() {
    var params = new URLSearchParams(window.location.search);
    if (!params.has("hash")) {
        console.warn("The hash param is missing.");
        rawHash = "0";
        hash = 0;
    }
    else {
        rawHash = params.get("hash");
        hash = parseInt(rawHash.substr(0, 8), 16);
    }
    noiseSeed(hash);
    randomSeed(hash);
    rand = sfc32(0, 4, hash, 1);
    for (let i = 0; i < 10; ++i) {
        rand();
    }
    var width = parseInt(params.get("width") ?? "1024");
    var height = parseInt(params.get("height") ?? "1024");
    createCanvas(width, height);
    pixelDensity(1);
    day = () => randint(0, 31) + 1;
    hour = () => randint(0, 24);
    minute = () => randint(0, 60);
    millis = () => randint(0, 1000);
    month = () => randint(0, 12) + 1;
    second = () => randint(0, 60);
    year = () => randint(0, 8192);
}
function saveHashCanvas(name) {
    saveCanvas(`${name}_${rawHash}`, "png");
}
function hx() { return randint(0, width); }
function hy() { return randint(0, height); }
