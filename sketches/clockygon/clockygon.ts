/**
 * @name Clockygon
 * @author Peťo Jaško <jasko at mail.muni.cz>
 **/

/// <reference path="../../lib/types/p5/index.d.ts" />
/// <reference path="../../lib/types/p5/global.d.ts" />

/* creative constraint: Využij goniometrické funkce pro určení barvy. */
// PJ

const mod = (n, m) => ((n % m) + m) % m // mathematically correct modulo
const cartesian = (theta, r): [number, number] => [r * sin(theta), r * cos(theta)]

const draw_polygon = (vertices: number, radius: number) => {
  beginShape()
  for (let i = 0; i < vertices; ++i)
    vertex(...cartesian(i * TAU / vertices, radius))
  endShape(CLOSE)
}

const translated = (x: number, y: number, callback: () => void) => {
  push()
  translate(x, y)
  callback()
  pop()
}

const rotated = (angle: number, callback: () => void) => {
  push()
  rotate(angle)
  callback()
  pop()
}

const scaled = (x: number, y: number, callback: () => void) => {
  push()
  scale(x, y)
  callback()
  pop()
}

class AbstractClock {
  readonly #hours: number // 6-hour clock
  readonly #minutes: number
  readonly #quadrant: number
  readonly #color;
  readonly #secondary;
  constructor(hours: number, minutes: number) {
    this.#quadrant = Math.trunc((hours - 1) / 6) + 1
    this.#hours = mod(hours - 1, 6) + 1
    this.#minutes = minutes
    this.#color =
      color(map(sin(-HALF_PI + TAU * hours / 24), -1, 1, 190, 430) % 360, 100, 100)
    this.#secondary =
      color(map(sin(-HALF_PI + TAU * hours / 24), -1, 1, 190, 430) % 360 - 15, 100, 100)
  }

  public draw(size: number = 100) {
    push()
    stroke(this.#color)
    strokeWeight(10).noFill()
    this.draw_hours(size)
    stroke(this.#secondary)
    this.draw_minutes(size)
    noStroke().fill(this.#color)
    this.draw_meridiam(size)
    pop()
  }

  private draw_hours(size: number) {
    if (this.#hours === 2) {
      [-1, 1].forEach(sign => translated(sign * size / 4, 0, () => draw_polygon(2, size)))
    } else {
      draw_polygon(this.#hours === 1 ? 2 : this.#hours, size)
    }
  }

  private draw_minutes(size: number) {
    const draw_minute_quadrant =
      () => scaled(1.31, 1, () => rotated(HALF_PI, () => draw_polygon(2, size)))

    const minute_quadrant = Math.floor(this.#minutes / 15)
    switch (minute_quadrant) {
      case 0:
        draw_minute_quadrant()
        break
      case 3: // draw an x
        [-1, 1].forEach(sign => rotated(sign * QUARTER_PI, draw_minute_quadrant))
        break
      default:
        [-1, 1, 0]
          .slice(0, minute_quadrant + 1)
          .forEach(sign => translated(0, sign * size * .3, draw_minute_quadrant))
    }
    const CIRCLE_SIZE = 3
    push()
    fill(this.#color).noStroke()
    translated(-8 * CIRCLE_SIZE, 1.3 * size, () => {
      let mins = this.#minutes % 15
      for (let j = 0; j < 3 && mins; ++j)
        for (let i = 0; i < 5 && mins; ++i, --mins)
          circle(i * 4 * CIRCLE_SIZE, j * 4 * CIRCLE_SIZE, CIRCLE_SIZE)
    })
    pop()
  }

  private draw_meridiam(size: number) {
    textSize(.13 * size)
    text(this.#quadrant < 3 ? 'a.m.' : 'p.m.', size, .5 * -size)
  }
}

let hour_slider;
let minute_slider;

function setup() {
  createHashCanvas()

  // hour_slider = createSlider(0, 23, hour(), 1)
  // hour_slider.position(30, 10)
  // minute_slider = createSlider(0, 59, minute(), 1)
  // minute_slider.position(30, 40)

  frameRate(30).colorMode('hsb').fill('white').noStroke().background('#111111')
  drawHash();
  saveHashCanvas("clockygon");
}

function drawHash() {
  blendMode(BLEND)
  background('#111111')

  let h = randint(0, 24);
  let m = randint(0, 60);

  // text(h, 10, 22)
  // text(m, 10, 52)

  blendMode(ADD)
  translate(width / 2, height / 2)
  new AbstractClock(h, m).draw(min(width / 2, height / 2) * 0.67)
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight)
}
