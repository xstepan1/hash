/**
 * @name Portal Core Clock
 * @author Hana Tokárová <484176 at mail.muni.cz>
 **/

// Hana Tokarova - 484176
// Portal Core Clock - Inspired by game Portal

color coreColor; // Color of the core
int slices = 21;                                // Number of lines in circle
int size = 300;                                 // Diameter of circle
float x; 										// X coordinate for line
float y; 										// Y coordinate for line

void setupHash() {
	// size(500, 400);
	coreColor = color(0, 0, random(50, 200));
	background(0);
}

void drawHash() {
	translate(width / 2, height / 2);
	scale(min(width / 500, height / 400));
	
	// Computes rotation and core for minutes
	pushMatrix();	
	rotate(hminute() * radians(6) + hsecond() * radians(6) / 60); 
	drawCore(size, 0);
	popMatrix();
	
	// Computes rotation and core for hours
	pushMatrix();
	rotate((hhour() % 12) * radians(30) + hminute() * radians(30) / 60 + hsecond() * radians(30) / (60 * 60)); 
	drawCore(size - 125, 1);
	popMatrix();
	
	// Computes core eye
	fill(coreColor);
	circle(0, 0, size - 250);
}

void drawCore(float diameter, int version) {
	strokeWeight(5);
	fill(0);
	circle(0, 0, diameter);
	
	// Computes x coordinate based on version
	x = (version == 0) ? - diameter / 2 + 7 : - diameter / 2 - 62.5 + 7;
	
	for (int i = 0; i <= slices + 1; i++) {
		stroke(255);
		
		// Computes y coordinate based on Pythagorean theorem
		y = sqrt(pow(diameter / 2, 2) - pow(x, 2)); 

		// Drawing outer / inner circle based on version
		if (i > slices / 2 + 1 && version == 0) {
			line(x, y, x, - y);
		}
		if (i < slices / 2 + 1 && version == 1) {
			line(x, y, x, - y);
		} 
		
		// Coloring hand watch
		if (i == slices / 2 + 1) {
			stroke(coreColor);
			line(x, - y, x, 0);
			stroke(255);
			line(x, y, x, 0);
		}
		x += (size - 14) / slices;
	}
	
	// Outline of core
	strokeWeight(8);
	stroke(0);
	noFill();
	circle(0, 0, diameter);
}

void keyPressed() {
	if (key == 's') saveFrame("hana_tokarova_portal_core_clock_##.png");
}
