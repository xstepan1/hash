/**
 * @name Hashday
 * @author Adam Štěpánek <469242 at mail.muni.cz>
 **/

function setup() {
    createHashCanvas();
    const answer = 42;
    const cellSize = answer;
    const slowness = answer * 3;
    const waveSize = answer * 3;
    const displacement = answer * 2;
    noStroke();
    background(0);
    for (var y = 0; y < height / cellSize; ++y) {
        for (var x = 0; x < width / cellSize; ++x ) {
            var t = millis() / slowness;
            var n = noise((x + t) / waveSize, (y + t) / waveSize);
            fill(n * answer);
            var n2 = (noise((y + t) / displacement, (x + t) / displacement) * 2) - 1;
            square(x * cellSize + n2 * displacement, y * cellSize + n2 * displacement, cellSize * 0.98);
        }
    }
    saveHashCanvas("hashday");
}
