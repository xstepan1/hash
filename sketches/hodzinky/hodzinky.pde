/**
 * @name Hodzinky
 * @author Julka Gonová <456129 at mail.muni.cz>
 **/

// Julka Gonova
// Creative constraints:
// Vytvoř nekonečný pohyb. - used
// Vytvoř vlastní gravitaci - used
// Udělej z toho hru.


float x1, y1, x2, y2;

float cenX;
float cenY;
float radCif = 300;
float radHour = 200;
float radMin = 250;
float elX, elY;
int counter = 0;
int inc = 1;
int minute;
int hour;
float timeOffset = -PI/2;
boolean isNight = false;
color gray = #CACACA;
color sun = #ffdf3d;
color dark = #071522;

String timestamp;


void setupHash() {
  // size(1000, 1000);
  cenX = 0;
  cenY = 0;
  translate(width / 2, height /2);
  scale(min(width / 1000, height / 1000));
  background(20);
  minute = hminute();
  hour = hhour();
}

float minuteAngle(int minute) {
  return (2*PI)/360 * 6 * minute;
}

float hourAngle(int hour) {
  return (2*PI)/360 * 30 * hour;
}
void drawHash() {
  timestamp = year() + "-" + minute() + "-" + millis();
  frameRate(60);
  fill(33);
  rect(-width / 2, -height / 2, width, height);
  isNight = (hour > 16);
  if (isNight) {
    fill(dark);
  } else {
    fill(255);
  }
  ellipse(cenX, cenY, radCif*2, 2*radCif);
  
  x1 = cenX + radHour * cos(timeOffset + hourAngle(hour));
  y1 = cenY + radHour * sin(timeOffset + hourAngle(hour));

  x2 = cenX + radMin * cos(timeOffset + minuteAngle(minute));
  y2 = cenY + radMin * sin(timeOffset + minuteAngle(minute));
  float steps = frameRate;
  noStroke();
  beginShape();
  vertex(x1, y1);
  bezierVertex(cenX, cenY, cenX, cenY, x2, y2);
  endShape();
  stroke(0);
  
  elX = bezierPoint(x1, x1, cenX-radHour/2, x2, counter/steps);
  elY = bezierPoint(y1, y1, cenY-radHour/2, y2, counter/steps);
  noStroke();
  int radMarker = 20;
  if (isNight) {   
    radMarker = 50;
    fill(gray);
  } else {
    fill(sun);
    radMarker = 70;
  }
  circle(elX, elY, radMarker);

  if (counter >= steps) {
    inc = -1;
  }
  if (counter == 0) {
    inc = 1;
  }
  counter += inc;
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("Julka_Gonova_pattern_" + timestamp + ".png");
}
