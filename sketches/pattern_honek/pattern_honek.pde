/**
 * @name Pattern
 * @author Martin Honěk <445350 at mail.muni.cz>
 **/

float offset = 10;
float rowCount = 9;
boolean backgroundNoise = false;
int mode = 2;
color[][] shadows;
color[][] rect_colors;
boolean crossRandRot = false;

void setupHash() {
  // size(730,730);
  
  colorMode(HSB, 360, 100, 100);
  shadows = new color[(int)rowCount][(int)rowCount];
  rect_colors = new color[(int)rowCount][(int)rowCount];
  translate(width / 2, height / 2);
  scale(min(width / 730, height / 730));
  translate(-365, -365);
}

void drawHash() {
  background(360,0,100);
  frameRate(1);
  
  for (int i = 0; i < rowCount; i++){
    for (int j = 0; j < rowCount; j++) {
        switch(mode) {
          case 0:
            shadows[i][j] = color(0,0,50);
            rect_colors[i][j] = color(0,0,95);

            break;
          case 1:
            float hue = random(180,360);
            shadows[i][j] = color(hue,30,50);
            rect_colors[i][j] = color(hue,50,40);
            break;
          case 2:
            float hue2 = random(180,360);
            shadows[i][j] = color(hue2,30,50);
            rect_colors[i][j] = color(hue2,50,40);
            break;
        }      
    }
  }

  
  noStroke();
  for (int i = 0; i < rowCount; i++) {
    for (int j = 0; j < rowCount; j++) {
      fill(shadows[i][j]);
      rect(i * 70 + 55 + offset,j * 70 + 55 + offset, 50,50); 
    }
  }
  
  filter(BLUR,4);
    
  if (backgroundNoise) {  
    stroke(185,30,80);
    for (int i = 0; i < 100; i++) {
      line(random(width), random(width), random(width), random(width));
    }
  }

  for (int i = 0; i < rowCount; i++) {
    for (int j = 0; j < rowCount; j++) {
      
      noStroke();
      fill(0,0,95);
      //fill(rect_colors[i][j]);
      rect(i * 70 + 55,j * 70 + 55, 50,50);
      if (mode < 2) {
        stroke(0,0,80);
        line(i * 70 + 55 + 5, j * 70 + 55 + 25, i * 70 + 55 + 45, j * 70 + 55 + 25);
        line(i * 70 + 55 + 25, j * 70 + 55 + 5, i * 70 + 55 + 25, j * 70 + 55 + 45);
      } else {
        drawPolygon(i * 70 + 55,j * 70 + 55,i, j);
      }
    }
  }
  noFill();
  rect(0,0, width - 1, height - 1);
  fill(0,0,0); 
}

void drawPolygon(float leftCornerX, float leftCornerY, int i_, int j_) {
  fill(0,0,0);
  float[] x = new float[8];
  float[] y = new float[8];
  for (int i = 1; i <= 8;i++) {
      x[i- 1] = cos((2*i*PI)/8 + radians(45 / 2)) * 20;
      y[i - 1] = sin((2*i*PI)/8 + radians(45 / 2)) * 20;
  }
  
  int a = 100;
  for (int i = 0; i < 8; i++) {
      int i2 = i+1;
      if (i2 >= 8) {
        i2 = 0;
      }
      
      int iC = i_;
      int jC = j_;
      switch(i) {
        case 0:
          jC += 1;
          break;
        case 1:
          jC += 1;
          iC -= 1;
          break;
        case 2:
          iC -= 1;
          break;
        case 3:
          iC -= 1;
          jC -= 1;
          break;
        case 4:
          jC -= 1;
          break;
       case 5:
          jC -= 1;
          iC += 1;
          break;
       case 6:
          iC += 1;
          break;
       case 7:
          iC += 1;
          jC += 1;
          break;
      }
      
      if (jC < rowCount && iC < rowCount - 1 && jC >= 0 && iC >= 0) {
        color c = rect_colors[iC][jC];
        fill(hue(c), 50,70);
      } else {
        fill(rect_colors[i_][j_]);
      }
      
      triangle(leftCornerX + 25,leftCornerY + 25, x[i] + leftCornerX + 25, y[i] + leftCornerY + 25, x[i2] + leftCornerX + 25, y[i2] + leftCornerY + 25);
  }
}

void keyPressed() {
  if (key == 's') saveFrame("Random and For ####.png");
  if (key == 'q') backgroundNoise = !backgroundNoise;
  switch(key) {
    case 'w':
      mode = 0;
      break;
    case 'e':
      mode = 1;
      break;
     case 'r':
      mode = 2;
      break;
     case 't':
      mode = 3;
      break;
  }
}
