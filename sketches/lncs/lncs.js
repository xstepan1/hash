var waitCount = 30;
setup = function() {
    canvas = createHashCanvas(600, 600, WEBGL);
    pixelDensity(max(hashWidth / 600, hashHeight / 600));
    canvas.position(345, 50);
    canvas.background(0);
    setupVariables();
    
    moodColour = randint(0, 100) <= 42 ? 1 : -1;
    changeColourMood(moodColour);

    let polaroidCol = randint(0, 100) <= 42 ? 1 : -1;
    pickPolaroidCol(polaroidCol);

    let wordCount = Object.keys(jsonData).length;
    firstWord = jsonData[randint(0, wordCount).toString()].En;
    secondWord = jsonData[randint(0, wordCount).toString()].En;
    thirdWord = jsonData[randint(0, wordCount).toString()].En;

    validateWords();
    fillAllEmotions();
    makeSumOfOnes();
    averageEmotions();
    generateErrorText();

    //Precalculate the city centre
    calculateCityCentreBuildingsPositions();
    calculateBaseColoursForCentreBuldings();

    // hack around phase one
    generate = true;
    pOne = true;
    createGroundTexture();
    isBTGenerated = true;
    for (var i = 0; i <= 100; ++i) {
        moveCameraPOne();
    }
    pOneSlideCounter = 300;
    slideCameraFade();

    // hack around phase two
    pTwoCounter = 80;
    phaseTwo();
    noLoop();
}

function drawHash() {
    canvas.background(0);
    lights();

    generateCityNetPOnePTwo();
    phaseThree();
    generateCityNetPThree();

    // THE CITY
    generateOutskirtsNet()

    // THE GROUND
    texture(planeRoadsTexture);
    plane(600, 600);
    createHashPolaroid();
}
draw = drawHash;

function createHashPolaroid() {
    var large = 2;    //minimum for project deadline 4.2
    polaroid = createGraphics(700 * large, 840 * large);
    polaroid.background(polaroidColour);
    push();
    polaroid.fill(0);
    polaroid.rect(50 * large, 50 * large, 600 * large, 600 * large);
    pop();
    polaroid.copy(canvas, 0, 0, 600, 600, 50 * large, 50 * large, 600 * large, 600 * large);
    
    polaroid.fill(polaroidText);
    polaroid.textFont("Courier New");
    polaroid.textSize(18 * large);
    polaroid.textAlign(CENTER);
    polaroid.text(firstWord + ". " + secondWord + ". " + thirdWord + ".", 350 * large, 730 * large)
    
    saveHashGraphics("lncs", polaroid);
    polaroid.remove();
}
