/**
 * @name Geometric
 * @author Peťo Jaško <jasko at mail.muni.cz>
 **/

let colors = null;

function setup() {
  createHashCanvas()
  rectMode(CENTER).noFill().noLoop()//.frameRate(30)
  blendMode(MULTIPLY)
  colorMode('hsb')

  colors = ['#00fddc', '#ff5666', '#ffccc9'].map(hex => {
    const [H, S, B] = [hue, saturation, brightness].map(f => f(hex))
    return color(random(0, 360), random(50, 100), B)
  })
  drawHash()
  saveHashCanvas("geometric")
}

const L = 50

const get_corners = (a) => [[0, 0], [a, 0], [a, a], [0, a]]
const corners = get_corners(L).concat(get_corners(L))

const _triangle = (c) => triangle(...corners.slice(c, c + 3).flat())
const _quarter = (c) => {
  const [x, y] = corners[c + 1]
  const start = (1 + c) * HALF_PI
  return arc(x, y, 2*L, 2*L, start, start + HALF_PI)
}

function* CornerIx(opposite_chance = .0) {
  let i = ~~random(4)
  while (true) {
    i = ~~random(i + 1, i + (random() < opposite_chance ? 3 : 2)) % 4
    yield i
  }
}

function drawHash() {
  background('#ffffff')
  noStroke()

  for (let i = 0; i < width; i += L) {
    for (let j = 0; j < height; j += L) {
      const corner = CornerIx()
      push()
      translate(i, j)

      fill(random(colors))
      ;(random() < .7 ? _triangle : _quarter)(corner.next().value)

      if (random() < .5) {
        fill(random(colors))
        _triangle(corner.next().value)
      }

      pop()
    }
  }
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight)
}
