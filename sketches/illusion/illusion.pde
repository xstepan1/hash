/**
 * @name Illusion
 * @author Honza Šafařík <tony.safarik at mail.muni.cz>
 **/

//SimpleSpiral spiral = new SimpleSpiral();
Circles circles;
Dots dots;
boolean switchFace = true;


void setupHash() {
  // size(900, 900);
  background(#262326);
  translate(width / 2, height / 2);
  scale(min(width / 900, height / 900));
  circles = new Circles();
  dots = new Dots();
}

void drawHash() {
  if (switchFace) {
    circles.draw();
  } else {
    dots.draw();
  }
}

void keyPressed() {
  if (key == 'a') {
    switchFace = !switchFace;
  }
  if (key == 's') {
    String time;
    if (switchFace) {
      time = circles.getTime();
    } else {
      time = month() + "." + day() + "T" + hour() + ":" + minute() + ":" + second();
    }
    
    saveFrame("honza_safarik_illusion_" + time + ".png");
  }
  
}
