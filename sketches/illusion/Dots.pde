class Dots {
  
  color bg = color(#D9A23D);
  color primary = color(#5B338C);
  color secondary = color(#A24936);
  color white = color(#FEFEFE);
  color black = color(#262326);
  
  public void draw() {
    background(black);
    strokeWeight(10);
    stroke(white, 80);
    for (int i = 1; i <= 6; i++) {
      line((width/7)*i, 0, (width/7)*i, height);
    }

    for (int i = 1; i <= 10; i++) {
      line(0, (height/11)*i, width, (height/11)*i);
    }

    for (int i = 1; i <= 10; i++) {
      for (int j = 1; j <= 6; j++) {
        stroke(white);
        strokeWeight(5);
        int currentIndex = ((i-1)*6) + (j-1);
        if (currentIndex == hhour() && currentIndex == hsecond()) {
          fill(secondary);
        } else if (currentIndex == hminute() && currentIndex == hsecond()) {
          fill(primary);
        } else if (currentIndex == hsecond()) {
          fill(black);
        } else {
          fill(white);
        }
        circle((width/7)*j, (height/11)*i, 20);
      }
    }
  }
}
