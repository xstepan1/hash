class Circles {

  int millisOffset;
  color bg = color(#D9A23D);
  color primary = color(#5B338C);
  color secondary = color(#A24936);
  color white = color(#FEFEFE);
  color black = color(#262326);

  float second;
  float minute;
  float hour;
  float day;
  float month;

  public Circles() {
    int currentSecond = hsecond();
    while (currentSecond == hsecond()) {
      this.millisOffset = hmillis();
    }
  }

  public String getTime() {
    return (int) month + "." + (int) day + "T" + (int) hour + ":" + (int) minute + ":" + (int) second;
  }

  public void draw() {
    background(this.bg);

    // ACTUAL DATA
    second = hsecond() + map((hmillis()-millisOffset) % 1000, 0, 1000, 0, 1);
    minute = hminute() + map(hsecond(), 0, 60, 0, 1);
    hour = hhour()%12 + map(hminute(), 0, 60, 0, 1);
    day = hday() + map(hhour(), 0, 24, 0, 1);
    month = hmonth() + map(hday(), 1, 31, 0, 1);

    // Mocked data
    //noiseSeed(second());
    //second = map(noise(second()), 0, 1, 0, 59);
    //minute = map(noise(minute()), 0, 1, 0, 59);
    //hour = map(noise(hour()), 0, 1, 0, 12);
    //day = map(noise(day()), 0, 1, 1, 31);
    //month = map(noise(month()), 0, 1, 1, 12);

    drawCircle(148, 0.486, true, 60, second);
    drawCircle(176, 0.6, false, 60, minute);
    drawCircle(210, 0.728, true, 12, hour);
    drawCircle(250, 0.917, false, 31, day);
    drawCircle(300, 1.115, true, 12, month);

    // Reversed
    //drawCircle(148, 0.486, true, 12, month);
    //drawCircle(176, 0.6, false, 31, day);
    //drawCircle(210, 0.728, true, 12, hour);
    //drawCircle(250, 0.917, false, 60, minute);
    //drawCircle(300, 1.115, true, 60, second);

    // Additional circle with correct values
    // in case I need another circle
    //drawCircle(360, 1.375, false, 1, 1);

    // element showing if its am or pm
    noStroke();

    // ACTUAL DATA
    circle(map(hhour(), 0, 24, -25, 25), 0, 25);

    // MOCKED DATA
    //circle(width/2 + map(hour * (second < 30 ? 1 : 2), 0, 24, -25, 25), height/2, 25);
    //hour = hour * (second < 30 ? 1 : 2);

    stroke(white);
    strokeWeight(7);
    line(0, -25, 0, 25);
  }

  public void drawSymbol(float size, boolean changeDirection) {
    pushMatrix();
    fill(primary);
    stroke(black);
    strokeWeight(4);
    translate(0, 25);
    if (changeDirection) {
      scale(-1, 1);
    }
    scale(size);
    translate(-25, 0);
    beginShape();
    vertex(0, 0);
    vertex(50, 0);
    vertex(40, -20);
    vertex(69, -48);
    vertex(20, -50);
    vertex(30, -30);
    endShape();
    popMatrix();
  }

  public void drawSymbol2(float size, boolean changeDirection, color clr) {
    pushMatrix();
    fill(clr);
    strokeWeight(2);
    translate(0, 25);
    if (changeDirection) {
      scale(-1, 1);
    }
    scale(size);
    //scale(map(mouseX, 0, width, -2, 2),1);
    //println(map(mouseX, 0, width, -2, 2));
    translate(-25, 0);
    stroke(clr);
    line(0, 0, 69, -48);
    stroke(black);
    beginShape();
    vertex(0, 0);
    vertex(50, 0);
    vertex(40, -20);
    vertex(69, -48);
    endShape();
    stroke(white);
    beginShape();
    vertex(69, -48);
    vertex(20, -50);
    vertex(30, -30);
    vertex(0, 0);
    endShape();
    popMatrix();
  }

  public void drawCircle(float radius, float size, boolean changeDirection, float maxRotation, float rotated) {
    int rotation = 0;
    int numOfSymbols = 30;
    pushMatrix();
    rotate(radians((360/maxRotation)*rotated));
    for (int i = 0; i < numOfSymbols; i++) {
      pushMatrix();
      rotate(radians(rotation));
      rotation += 360/numOfSymbols;
      translate(0, -radius);
      if (i == 0) {
        drawSymbol2(size, changeDirection, secondary);
      } else {
        drawSymbol2(size, changeDirection, primary);
      }
      popMatrix();
    }
    popMatrix();
  }
}
