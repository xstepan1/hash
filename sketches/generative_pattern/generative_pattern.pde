/**
 * @name Generative Pattern
 * @author Eva Kuhejdová <485343 at mail.muni.cz>
 **/

String timestamp;
int[][] hue;
int[][] saturation;
int[][] rand;
int[] r_alpha = new int[100];
int[] r_stroke = new int[100];
int[] r_height = new int[100];


void setupHash() {
  // size(800, 800);

  frameRate(5);
  // timestamp = year() + "-" + minute() + "-" + millis();
  
  background(33);
  colorMode(HSB, 360, 100, 100, 100);
  
  hue = new int[width / 8][height / 8];
  saturation = new int[width / 8][height / 8];
  rand = new int[width / 8 + 1][height / 8 + 1];
  for (int i = 0; i < width / 8; i++){
    for (int j = 0; j < height / 8; j++){
      hue[i][j] = int(random(40, 60));
      saturation[i][j] = int(random(0, 100));   
      rand[i][j] = int(random(1,3));
    }
  }
  
  for (int i = 0; i < 100; i++){
    r_alpha[i] = int(random(0, 100));
    r_stroke[i] = int(random(0, 15));
    r_height[i] = int(random(0, height));
  }
}


void drawHash() {
  fill(0, 100);
  rect(0, 0, width, height);
  //stroke(360, 100, 0, 100);
  //strokeWeight(1);
  
  noStroke();
  

  for (int i = 0; i < width; i = i+100){
    for (int j = 0; j < height; j = j+100){
      fill(hue[i/100][j/100], saturation[i/100][j/100], 100, 100);
      square(i, j, 100);
    }
  }
  
  for (int i = 0; i < width; i = i+8){
    for (int j = 0; j < height; j = j+8){
      //fill(hue[i/8][j/8], saturation[i/8][j/8], 100, 100);
      //fill(60 - j/16, 100, 100, 100); //rainbow
      
      if (rand[i/8][j/8] == 2) {
        fill(60 - j/16, 100, 100, 100);
        square(i, j, 8);
      }
      if (rand[i/8][j/8] == 1) {
        fill(60 - i/16, 100, 100, 100);
        circle(i, j, 8);
      }
      
      
      //forest
      
      //if (rand[i/8][j/8] == 2) {
      //  triangle(i + 0, j - 8, 
      //  i + 8 * sqrt(3)/2, j + 8 * 0.5, 
      //  i - 8 * sqrt(3)/2, j + 8 * 0.5);
      //  triangle(i+4 + 0, j+4 - 8, 
      //  i+4 + 8 * sqrt(3)/2, j+4 + 8 * 0.5, 
      //  i+4 - 8 * sqrt(3)/2, j+4 + 8 * 0.5);
      //}
    }
  }
  
  
  //slime
  
  //for (int i = 0; i < width; i = i+8) {
  //  stroke(hue[i/8][1], 100, 0, r_alpha[i/8]);
  //  strokeWeight(r_stroke[i/8]);
  //  line(i, 0, i, r_height[i/8]);
  //}
  
  //diagonals
  //for (int i = 0; i < width; i = i+8) {
  //  stroke(hue[i/8][1], 100, 0, r_alpha[i/8]);
  //  strokeWeight(10);
  //  line(i, r_height[i/8], r_height[i/8], i);
  //}
  
  
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("Eva_Kuhejdova_geometric_pattern-" + timestamp + "-####.png");
  
  if (key == 'c' || key == 'C') {
    for (int i = 0; i < 100; i++){
    for (int j = 0; j < 100; j++){
      hue[i][j] = int(random(40, 60));
      saturation[i][j] = int(random(0, 100));   
      rand[i][j] = int(random(1,3));
      }
    }
  }
  
  if (key == 'm' || key == 'M') {
    for (int i = 0; i < 100; i++){
    r_alpha[i] = int(random(0, 100));
    r_stroke[i] = int(random(0, 15));
    r_height[i] = int(random(0, height));
    }
  }
}
