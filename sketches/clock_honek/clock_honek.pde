/**
 * @name Clock
 * @author Martin Honěk <445350 at mail.muni.cz>
 **/

float scale = 1;
int cx, cy;
float secondsRadius;
float minutesRadius;
float hoursRadius;
float noiseRadius;
float cRadius;
float clockDiameter;
IntList primeNumbers;  
int num = 6;

boolean noiseHourHand = true;

void setupHash() {
  // size(1024, 1024);
  background(0);

  stroke(255);
  int radius = min(width, height) / 2;
  secondsRadius = radius * 0.72;
  noiseRadius = radius * 0.85;
  minutesRadius = radius * 0.67;
  hoursRadius = radius * 0.50;
  clockDiameter = radius * 1.8;
  cRadius = radius * 0.50;

  primeNumbers = primeNumbersBruteForce(1000);

  cx = width / 2;
  cy = height / 2;
}

float k = 1.0;
float t = 0;
void drawHash() {

  background(#001222);
  // subtract HALF_PI to make them start at the top
  float s = map(hsecond(), 0, 60, 0, TWO_PI) - HALF_PI;
  float m = map(hminute() + norm(hsecond(), 0, 60), 0, 60, 0, TWO_PI) - HALF_PI; 
  float h = map(hhour() + norm(hminute(), 0, 60), 0, 24, 0, TWO_PI * 2) - HALF_PI;

  stroke(255);

  fill(255);
  circle(cx, cy, 30);
  noFill();

  // Draw the minute ticks
  strokeWeight(2);
  for (int a = 0; a < 360; a+=6) {
    float angle = radians(a);
    float x = cx + cos(angle) * secondsRadius;
    float y = cy + sin(angle) * secondsRadius;
    if (a % 90 == 0) {
      circle(x, y, 20);
    } else {
      circle(x, y, 2);
    }
  }

  if (!noiseHourHand) {
    int tt = 0;
    stroke(168);
    strokeWeight(0.5);
    int q = 25;
    int p = (int) (cRadius / q);
    for (int i = 0; i < q; i++) {

      beginShape();
      for (int a = 0; a < 720; a++) {
        float xx = noise(tt);
        float prime = primeNumbers.get(i);
        xx = map(xx, 0, 1, -prime, prime);
        float angle = radians(a);
        float x = cx + cos(angle) * p * i;
        float y = cy + sin(angle) * p * i;
        vertex(x + xx, y + xx);   
        tt += 1;
      }
      endShape(CLOSE);
      tt += 1;
    }
  }

  drawBackground(s, m, h);
  translate(width/2, height/2);
  noStroke();
  fill(255);
}

void drawBackground(float s, float m, float h) { 

  if (noiseHourHand) {
    float cot = 0.3f;
    float rand = 5;
    noFill();
    stroke(#e76f51);
    strokeWeight(0.1);
    int q = 50;
    int p = (int) (noiseRadius / q);
    for (int j = 0; j < 100; j++) {  
      t = 0;
      float r = random(-rand, rand);
      beginShape();
      curveVertex(cx, cy);
      for (int i = 1; i < q; i++) {     
        float xx = noise(t);
        xx = map(xx, 0, 1, -cot - r, cot + r);
        curveVertex(cx + cos(s + xx / primeNumbers.get(i)) * p * i, cy + sin(s + xx / primeNumbers.get(i)) * p * i);
        t += 10;
      }
      endShape();
    }

    cot = 0.4f;
    rand = 15;
    stroke(#f4a261);
    q = 30;
    p = (int) (minutesRadius / q);
    for (int j = 0; j < 100; j++) {  
      t = 0;
      float r = random(-rand, rand);
      beginShape();
      curveVertex(cx, cy);
      for (int i = 1; i < q; i++) {     
        float xx = noise(t);
        xx = map(xx, 0, 1, -cot - r, cot + r);
        curveVertex(cx + cos(m + xx / primeNumbers.get(i)) * p * i, cy + sin(m + xx / primeNumbers.get(i)) * p * i);
        t += 10;
      }
      endShape();
    }

    stroke(#2a9d8f);
    cot = 0.8f;
    rand = 30;
    q = 25;
    p = (int) (hoursRadius / q);
    for (int j = 0; j < 500; j++) {  
      t = 0;
      float r = random(-rand, rand);
      beginShape();
      curveVertex(cx, cy);
      for (int i = 1; i < q; i++) {     
        float xx = noise(t);
        xx = map(xx, 0, 1, -cot - r, cot + r);
        curveVertex(cx + cos(h + xx / primeNumbers.get(i)) * p * i, cy + sin(h + xx / primeNumbers.get(i)) * p * i);
        t += 10;
      }
      endShape();
    }
  } else {
    stroke(#e76f51);
    strokeWeight(2);
    line(cx, cy, cx + cos(s) * secondsRadius, cy + sin(s) * secondsRadius);
    strokeWeight(4);
    stroke(#f4a261);
    line(cx, cy, cx + cos(m) * minutesRadius, cy + sin(m) * minutesRadius);

    strokeWeight(8);
    stroke(#f4a261);
    line(cx, cy, cx + cos(h) * hoursRadius, cy + sin(h) * hoursRadius);
  }
}

IntList primeNumbersBruteForce(int n) {
  IntList primeNumbers = new IntList();
  for (int i = 2; i <= n; i++) {
    if (isPrimeBruteForce(i)) {
      primeNumbers.append(i);
    }
  }
  return primeNumbers;
}

boolean isPrimeBruteForce(int number) {
  for (int i = 2; i < number; i++) {
    if (number % i == 0) {
      return false;
    }
  }
  return true;
}

boolean isPrime(int p)
{
  if (p<2)
    return false;
  if (p == 2 || p == 3)
    return true;
  if (p%2 == 0 || p%3 == 0)
    return false;
  int maxcheck = floor(sqrt(p))+1;
  for (int f = 6; f<maxcheck; f += 6) {
    if (p%(f-1) == 0 || p%(f+1)==0) //Uses the fact that all primes are one more or one less than a multiple of 6 (past 2, 3)
      return false;
  }
  return true;
}


void keyPressed() {
  if (key == 's') saveFrame("Random and For ####.png");
}
