/**
 * @name Dandelions
 * @author Eva Kuhejdová <485343 at mail.muni.cz>
 **/

String timestamp;

void setupHash() {
  // size(800, 800);
  //fullScreen();
  hasManyFrames = true;
  background(33);
  colorMode(HSB, 360, 100, 100, 100);
  // frameRate(10);
  // timestamp = year() + "-" + minute() + "-" + millis();
}


void drawHash() {
  noStroke();
  fill(0, 5);
  rect(0, 0, width, height);
  
  float shade = random(0,255);
  stroke(0, 0, shade, 70);
  strokeWeight(15);
  
  if(random(1) < 0.10) {
    stroke(#497cc3);
  }
  
  for (int i = 0; i < 2; i++) {
    
    float r = random(10, 100);
    
    float y = int(random(30)) * height/30;
    float x = int(random(30)) * width/30;
    
    // one big dandelion
    float length = map(hx(), 0, width, 0, width/4);
       
    line(hx(), hy(), x+length, y);
    ellipse(x+length, y, r, r);
    
    // line of dandelions
    //float length = map(mouseX, 0, width, 0, 1);

    //line(mouseX, width, mouseX, length*r*r);
    //ellipse(mouseX, length*r*r, r, r);
  }
}

void keyPressed() {
  if (key == 's' || key == 'S') saveFrame("Eva_Kuhejdova_dandelions-" + timestamp + "-####.png");
}
