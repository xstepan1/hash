/**
 * @name Geometric
 * @author Franta Holubec <xholubec at mail.muni.cz>
 **/

import java.util.ArrayList; 

String timestamp;
color colorList[] = { #ec3c32, #09cbc3, #ffffff, #404f56, #8fcee2, #494337 };
ArrayList<Part> parts = new ArrayList<Part>(); 
float rot;

public class Part {
  int x;
  int y;
  color colA;
  color colB;
  color colC;
  color colD;
  color colE;
  float rotA;
  float rotB;
  int dirA;
  int dirB;
  
  public Part(int x, int y) {
    this.x = x;
    this.y = y;
    colA = randomCol();
    colB = randomCol();
    colC = randomCol();
    colD = randomCol();
    colE = randomCol();
    rotA = (int)random(0, 2) * HALF_PI;
    rotB = (int)random(0, 2) * HALF_PI;
    dirA = random(1) > .5 ? -1 : 1;
    dirB = -dirA;
  }
}


void setupHash() {
  // size(1920, 1080);
  background(20);
  frameRate(30);
  colorMode(HSB, 360, 100, 100, 100);
  timestamp = hyear() + nf(hmonth(), 2) + nf(hday(), 2) + "-"  + nf(hhour(), 2) + nf(hminute(), 2) + nf(hsecond(), 2);
  noStroke();
  
  for (int x = 60; x <= width; x += 120) {
      for (int y = 60; y <= height; y += 120) {
        parts.add(new Part(x,y));
      }
    }
}

color randomCol(){
  return colorList[int(random(colorList.length))];
}

void drawHash(){
  rot += 0.1;
  
  for (Part part : parts) 
  { 
    float finRotA = part.dirA * (part.rotA + rot);
    float finRotB = part.dirB * (part.rotB + rot);
    
    float startX = part.x - 60;
    float startY = part.y - 60;
    
    fill(part.colA);
    square(startX, startY, 120);
    
    fill(part.colB);
    arc(part.x, part.y, 120, 120, finRotA , PI + finRotA);
    fill(part.colC);
    arc(part.x, part.y, 120, 120, -PI + finRotA, finRotA);
    
    fill(part.colD);
    arc(part.x, part.y, 60, 60, finRotB, PI + finRotB);
    fill(part.colE);
    arc(part.x, part.y, 60, 60, -PI + finRotB, finRotB);
  }
}


// function called when any key is pressed by user
void keyPressed() {
  // if 's' key is pressed, save the current frame 
  if (key == 's' || key == 'S') saveFrame("Franta_Holubec_Geometric-" + timestamp + "-####.png");
}
