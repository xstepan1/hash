asciidoctor-pdf `
	-a pdf-theme=./theme.yml `
	-a pdf-fontsdir="C:\dev\fonts;GEM_FONTS_DIR" `
	-a source-highlighter=rouge `
	-a rouge-style=monokai `
	-o HASH.pdf ../README.ad
