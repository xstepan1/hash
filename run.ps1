param(
    [string] $path,
    [string] $seed,
    [string] $hash,
    [int] $width = 1920,
    [int] $height = 1080,
    [switch] $debug = $false)

$dockerTag = "hash"

$inputCount = 0;
if (0 -ne $path.Length) { $inputCount++ }
if (0 -ne $seed.Length) { $inputCount++ }
if (0 -ne $hash.Length) { $inputCount++ }
if ($inputCount -ne 1) {
    Write-Host "Exactly one of '-Path', '-Seed', or '-Hash' options is required."
    exit 1
}

# create './out'
if (-Not (Test-Path -PathType Container -Path ./out)) {
    New-Item -ItemType Directory -Path ./out
    if (-Not($?)) { 
        Write-Host "Failed to create the output directory."
        exit 1
    }
}

# get the sha256 fingerprint
if (0 -ne $path.Length) {
    $hash = (Get-FileHash -Path $path -Algorithm SHA256).Hash;
}
elseif (0 -ne $seed.Length) {
    $stream = ([IO.MemoryStream]::new([Text.Encoding]::UTF8.GetBytes($seed)))
    $hash = (Get-FileHash -InputStream $stream -Algorithm SHA256).Hash;
}
$hash = $hash.ToLower();

# verify that the image has been built
docker image inspect "$dockerTag" 2>&1 | Out-Null
if (-Not ($?)) {
    Write-Host "Please build the Docker image."
    exit 1
}

# create a container
if ($debug) {
    $container = (docker create -it --network none `
            -e WIDTH=$width -e HEIGHT=$height -e DEBUG=true "$dockerTag" /hash.sh $hash)
}
else {
    $container = (docker create -it --network none `
            -e WIDTH=$width -e HEIGHT=$height "$dockerTag" /hash.sh $hash)
}
if (-Not($?)) {
    Write-Host "Failed to create a Docker container."
    exit 1
}

# execute HASH
docker start -ai $container
$out = "./out/hash_${hash}.png"
docker cp ${container}:/out/hash.png $out
Write-Host "Saved to '$out'"

if ($debug) {
    $tmp = "$(Get-Location)\tmp"
    if (Test-Path $tmp) {
        Remove-Item -r $tmp
    }
    docker cp ${container}:/tmp $tmp
    Write-Host "Copied '/tmp to '$tmp'"
}

docker stop $container | Out-Null
docker rm $container | Out-Null
