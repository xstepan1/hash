import bpy
import math
import os

relief = os.environ["RELIEF"]
out = os.environ["OUT"]
width = float(os.environ["WIDTH"])
height = float(os.environ["HEIGHT"])

image = bpy.data.images.load(filepath=relief)
plane = bpy.data.objects["Plane"]
plane.scale = (1.0, height / width, 1.0)
plane.material_slots[0].material.node_tree.nodes["Image Texture"].image = image

bpy.data.scenes["Scene"].render.filepath = out
bpy.data.scenes["Scene"].render.resolution_x = width
bpy.data.scenes["Scene"].render.resolution_y = height
bpy.ops.render.render(write_still=True)
