import bpy
import math
import os

painting = os.environ["PAINTING"]
out = os.environ["OUT"]
width = float(os.environ["WIDTH"])
height = float(os.environ["HEIGHT"])

bpy.data.objects["Camera"].location.x *= math.sqrt(width / height)

image = bpy.data.images.load(filepath=painting)
canvas = bpy.data.objects["Canvas"]
canvas.material_slots[0].material.node_tree.nodes["Image Texture"].image = image

bpy.data.scenes["Scene"].render.filepath = out
bpy.data.scenes["Scene"].render.resolution_x = width
bpy.data.scenes["Scene"].render.resolution_y = height
bpy.ops.render.render(write_still=True)
