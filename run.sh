#!/bin/bash

while getopts ":p:s:f:W:H:Dh" opt; do
case $opt in
p)
    HASH=$(sha256sum "$OPTARG" | cut -d " " -f1)
;;
s)
    HASH=$(echo -n "$OPTARG" | sha256sum | cut -d " " -f1)
;;
f)
    HASH=$OPTARG
;;
W)
    WIDTH=$OPTARG
;;
H)
    HEIGHT=$OPTARG
;;
D)
    DEBUG=true
;;
? | h | *)
    echo >&2 << EOF
Usage: run.sh <-p PATH | -s SEED | -f SHA256 FINGERPRINT>

Options
-W  set width [1920]
-H  set height [1080]
-D  run in debug mode
-h show help
EOF
;;
esac
done

DOCKER_TAG="hash"

WIDTH=${WIDTH:-7016}
HEIGHT=${HEIGHT:-4960}
HASH=${HASH:-"0000000000000000000000000000000000000000000000000000000000000000"}

docker image inspect "$DOCKER_TAG" >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo >&2 "Please build the Docker image first."
    exit 1
fi

if [ -n $DEBUG ]; then
    CONTAINER=$(docker create -it --network none \
            -e WIDTH=$WIDTH -e HEIGHT=$HEIGHT -e DEBUG=true "$DOCKER_TAG" /hash.sh $HASH)
else
    CONTAINER=$(docker create -it --network none \
            -e WIDTH=$WIDTH -e HEIGHT=$HEIGHT "$DOCKER_TAG" /hash.sh $HASH)
fi

if [ $? -ne 0 ]; then
    echo >&2 "Failed to create a Docker container."
    exit 1
fi

docker start -ai $CONTAINER
OUT="$PWD/out/hash_${HASH}.png"
if [ ! -d "$PWD/out" ]; then
    mkdir "$PWD/out"
fi
docker cp "$CONTAINER:/out/hash.png" "$OUT"
echo "Saved to '$OUT'"

if [ -n $DEBUG ]; then
    TMP="$PWD/tmp"
    if [ -d "$TMP" ]; then
        rm -rf "$TMP"
    fi
    docker cp "$CONTAINER:/tmp" $TMP
    echo "Copied '/tmp' to '$TMP'"
fi

docker stop $CONTAINER >/dev/null
docker rm $CONTAINER >/dev/null
